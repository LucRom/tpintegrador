using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class Zombie_Controlador : MonoBehaviour
{
    [Header("Propiedades")]
    [SerializeField] public float daņoCamioneta;
    [SerializeField] public float daņoJugador;
    [SerializeField] float cooldownAtaqueCamioneta;
    private bool puedeAtacarCamioneta;
    private float timerAtaqueCamioneta;

    private void Start()
    {
        puedeAtacarCamioneta = true;
        timerAtaqueCamioneta = cooldownAtaqueCamioneta;
    }
    private void Update()
    {
        if (puedeAtacarCamioneta)
        {
            DetectarCamioneta();
        }
        else
        {
            TimerAtaqueCamioneta();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Camioneta"))
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoCamioneta(daņoCamioneta);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoJugador(daņoJugador);
        }
    }
    private void TimerAtaqueCamioneta()
    {
        timerAtaqueCamioneta -= Time.deltaTime;
        if (timerAtaqueCamioneta <= 0)
        {
            puedeAtacarCamioneta = true;
            timerAtaqueCamioneta = cooldownAtaqueCamioneta;
        }
    }
    private void DetectarCamioneta()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 1))
        {
            if (hit.transform.gameObject.CompareTag("Camioneta"))
            {
                hit.transform.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoCamioneta(daņoCamioneta);
                puedeAtacarCamioneta = false;
            }
        }
    }
}
