using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public class Controlador_Vida : MonoBehaviour
{
    [Header("Vida")]
    [SerializeField] public float vida;
    [SerializeField] bool tieneBarraDeVida;
    [SerializeField] bool barraDeVidaEstatica;
    [SerializeField] Image barraDeVida;
    [SerializeField] Image barraDeVidaFondo;
    public float vidaActual;
    private Camera camara;
    [Header("Efectos")]
    [SerializeField] bool tieneSangre;
    [SerializeField] VisualEffect sangre;
    [Header("Drops")]
    [SerializeField] bool dropItem;
    [SerializeField] List<GameObject> items = new List<GameObject>();
    [Header("Jefe 2")]
    public bool jefe2;
    [Header("Lvl3")]
    public bool lvl3;
    [SerializeField] float vidaDevuelta;
    private void Start()
    {
        if(tieneSangre)
        {
            sangre.Stop();
        }
        camara = Camera.main;
        vidaActual = vida;
    }
    private void Update()
    {
        if(tieneBarraDeVida && barraDeVidaEstatica == false)
        {
            barraDeVidaFondo.transform.rotation = Quaternion.LookRotation(transform.position - camara.transform.position);
            barraDeVida.transform.rotation = Quaternion.LookRotation(transform.position - camara.transform.position);
        }
    }
    public void RecibirDa�oDesaparecer(float da�o)
    {
        vidaActual -= da�o;
        if(tieneBarraDeVida)
        {
            barraDeVida.fillAmount = vidaActual / vida;
        }
        if (vidaActual <= 0)
        {
            if(dropItem)
            {
                DropItem();
            }
            if (jefe2)
            {
                GameManagerJefe2.enemigosEliminados++;
            }
            if (lvl3)
            {
                GameObject.Find("Pivote camioneta/jugador").GetComponent<Controlador_Vida>().vidaActual += vidaDevuelta;
                GameObject.Find("Pivote camioneta/jugador").GetComponent<Controlador_Vida>().ActualizarBarraDeVida();
            }
            Destroy(this.gameObject);
        }
    }
    public void RecibirDa�oOcultar(float da�o)
    {
        vidaActual -= da�o;
        if (tieneBarraDeVida)
        {
            barraDeVida.fillAmount = vidaActual / vida;
        }
        if (vidaActual <= 0)
        {
            gameObject.SetActive(false);
        }
    }
    public void RecibirDa�oJugador(float da�o)
    {
        vidaActual -= da�o;
        if (tieneBarraDeVida)
        {
            barraDeVida.fillAmount = vidaActual / vida;
        }
        if (vidaActual <= 0)
        {
            ManagerStats.instancia.DevolverDatosDefault();
            GameManager.gameOver = true;
            gameObject.SetActive(false);
        }
    }
    public void RecibirDa�oCamioneta(float da�o)
    {
        vidaActual -= da�o;
        if (tieneBarraDeVida)
        {
            barraDeVida.fillAmount = vidaActual / vida;
        }
        if (vidaActual <= 0 && gameObject.GetComponent<Camioneta_Movimiento>().segundaOportunidad == false)
        {
            ManagerStats.instancia.DevolverDatosDefault();
            GameManager.gameOver = true;
            gameObject.SetActive(false);
        }
        else if (vidaActual <= 0 && gameObject.GetComponent<Camioneta_Movimiento>().segundaOportunidad)
        {
            vidaActual = 150;
            barraDeVida.fillAmount = vidaActual / vida;
            gameObject.GetComponent<Camioneta_Movimiento>().segundaOportunidad = false;
        }
    }
    public void MostrarSangre(Transform posicion)
    {
        if (tieneSangre)
        {
            sangre.Play();
            sangre.gameObject.transform.position = posicion.position;
        }
    }
    public void RellenarBarraDeVida()
    {
        barraDeVida.fillAmount = 1;
    }
    private void DropItem()
    {
        int rnd = UnityEngine.Random.Range(0, 2);
        if (rnd == 1)
        {
            Instantiate(items[UnityEngine.Random.Range(0, items.Count)], transform.position, Quaternion.identity);
        }
    }
    public void MostrarBarraDeVida()
    {
        barraDeVida.gameObject.SetActive(true);
        barraDeVidaFondo.gameObject.SetActive(true);
        barraDeVida.fillAmount = vidaActual / vida;
    }
    public void OcultarBarraDeVida()
    {
        barraDeVida.gameObject.SetActive(false);
        barraDeVidaFondo.gameObject.SetActive(false);
    }
    public void ActualizarBarraDeVida()
    {
        barraDeVida.fillAmount = vidaActual / vida;
    }
}
