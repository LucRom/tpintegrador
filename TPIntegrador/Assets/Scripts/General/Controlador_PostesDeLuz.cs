using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_PostesDeLuz : MonoBehaviour
{
    [SerializeField] float velocidad;
    [SerializeField] Transform Spawn;
    void Update()
    {
        transform.Translate(-transform.forward * velocidad * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Limites"))
        {
            transform.position= Spawn.position;
        }
    }
}
