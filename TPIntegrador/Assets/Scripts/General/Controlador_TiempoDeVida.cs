using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Controlador_TiempoDeVida : MonoBehaviour
{
    [SerializeField] float tiempoDeVida;
    private float timer;
    void Start()
    {
        timer = tiempoDeVida;
    }
    void Update()
    {
        timer -= Time.deltaTime;
        if(timer <=0)
        {
            gameObject.SetActive(false);
        }
    }
}
