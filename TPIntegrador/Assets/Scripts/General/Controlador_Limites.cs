using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Limites : MonoBehaviour
{
    private float velocidadAux;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            velocidadAux = collision.gameObject.GetComponent<Player_Movimiento>().velocidadMod;
            collision.gameObject.GetComponent<Player_Movimiento>().velocidadMod = 7;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Player_Movimiento>().velocidadMod = velocidadAux;
        }
    }
}
