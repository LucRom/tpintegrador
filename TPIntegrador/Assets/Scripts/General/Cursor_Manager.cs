using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cursor_Manager : MonoBehaviour
{
    [SerializeField] Texture2D crosshair;
    [SerializeField] Texture2D flecha;
    private void Start()
    {
        Scene escenaActual = SceneManager.GetActiveScene();
        string nombreEscena = escenaActual.name;

        switch(nombreEscena)
        {
            case "Menu Principal":
                Cursor.SetCursor(flecha, new Vector2(0, 0), CursorMode.Auto);
                break;
            case "Lvl1":
                Cursor.SetCursor(crosshair, new Vector2(16, 16), CursorMode.Auto);
                break;
            case "Lvl2":
                Cursor.SetCursor(crosshair, new Vector2(16, 16), CursorMode.Auto);
                break;
            case "Lvl3":
                Cursor.SetCursor(crosshair, new Vector2(16, 16), CursorMode.Auto);
                break;
            case "Tienda":
                Cursor.SetCursor(flecha, new Vector2(0, 0), CursorMode.Auto);
                break;
            case "Jefe":
                Cursor.SetCursor(crosshair, new Vector2(16, 16), CursorMode.Auto);
                break;
            case "Jefe 2":
                Cursor.SetCursor(crosshair, new Vector2(16, 16), CursorMode.Auto);
                break;
        }
        
    }
}
