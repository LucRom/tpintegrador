using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Controlador_FinDeNivel : MonoBehaviour
{
    public bool alNivel;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Camioneta"))
        {
            ManagerStats.instancia.primeraPartida = false;
            ManagerStats.instancia.GuardarDatos();
            SceneManager.LoadScene("Tienda");
            GestorDeAudio.instancia.PausarSonido("MacheteFuriaMusic");
        }
        if (other.gameObject.CompareTag("Player") && alNivel)
        {
            ManagerStats.nivelesParaElJefe++;
            ManagerStats.nivelDeDificutlad++;
            ManagerStats.instancia.GuardarDatos();
            ManagerStats.instancia.CheckNivelEspecial();
            if (ManagerStats.nivelEspecial)
            {
                int rnd = UnityEngine.Random.Range(0, 2);
                if(rnd == 0)
                {
                    SceneManager.LoadScene("Jefe");
                }
                else
                {
                    SceneManager.LoadScene("Jefe 2");
                }

            }
            else
            {
                int rndNiveles = UnityEngine.Random.Range(0, 3);
                if(rndNiveles == 0)
                {
                    SceneManager.LoadScene("Lvl1");
                }
                else if(rndNiveles == 1)
                {
                    SceneManager.LoadScene("Lvl2");
                }
                else
                {
                    SceneManager.LoadScene("Lvl3");
                }
            }
        }
    }
}
