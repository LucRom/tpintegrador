using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Eventos_Animacion : MonoBehaviour
{
    [SerializeField] Animator ataqueMelee;
    public void Machete_DejoDeAtacar()
    {
        ataqueMelee.SetBool("EstaAtacando", false);
    }
}
