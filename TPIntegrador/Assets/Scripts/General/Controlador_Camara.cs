using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Camara : MonoBehaviour
{
    public GameObject Objetivo;
    public Vector3 offset;
    public bool seguirJugador;
    [SerializeField] GameObject limite1;
    [SerializeField] GameObject limite2;
    [SerializeField] GameObject limite3;
    [SerializeField] GameObject limite4;

    void Start()
    {
        if(seguirJugador)
        {
            Objetivo = GameObject.Find("Jugador");
            transform.position = new Vector3(Objetivo.transform.position.x, 18, Objetivo.transform.position.z);
            limite1.SetActive(false);
            limite2.SetActive(false);
            limite3.SetActive(false);
            limite4.SetActive(false);
        }
        else
        {
            Objetivo = GameObject.Find("Camioneta");
        }
        offset = transform.position - Objetivo.transform.position;
    }
    void LateUpdate()
    {
        if(Objetivo!= null)
        {
            transform.position = Objetivo.transform.position + offset;
        }
    }

}
