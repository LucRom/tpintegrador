using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Spawner : MonoBehaviour
{
    public GameObject objeto;
    public float cooldown;
    public float timer;
    void Start()
    {
        timer = cooldown;
    }
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Instantiate(objeto, transform);
            timer = cooldown;
        }
    }
}
