using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Controlador_HUD : MonoBehaviour
{
    [Header("Textos")]
    [SerializeField] TMPro.TMP_Text txtDinero;
    [SerializeField] TMPro.TMP_Text txtEstadoDelJuego;
    [SerializeField] TMPro.TMP_Text txtDistancia;
    [SerializeField] TMPro.TMP_Text txtDistanciaRecord;
    [SerializeField] TMPro.TMP_Text txtMunicion;
    [SerializeField] TMPro.TMP_Text txtMunicionSecundaria;
    [SerializeField] TMPro.TMP_Text txtContadorMadera;
    [Header("Extras")]
    [SerializeField] GameObject jugador;
    [SerializeField] GameObject camioneta;
    void Update()
    {
        txtDistancia.text = "Distancia: "+ Convert.ToInt32(camioneta.GetComponent<Camioneta_Movimiento>().distancia) + "KM";
        txtDistanciaRecord.text = "Distancia Record: " + Convert.ToInt32(camioneta.GetComponent<Camioneta_Movimiento>().distanciaRecord) + "KM";
        txtDinero.text = "$ " + jugador.GetComponent<Player_Dinero>().dinero.ToString();
        if (jugador.GetComponent<Player_Disparo>().escopeta)
        {
            txtMunicion.text = "Munici�n escopeta: " + jugador.GetComponent<Player_Disparo>().municionEscopeta;
        }
        else if (jugador.GetComponent<Player_Disparo>().lanzaCohete)
        {
            txtMunicion.text = "Munici�n Lanza Cohetes: " + jugador.GetComponent<Player_Disparo>().municionLanzaCohete;
        }
        else if (jugador.GetComponent<Player_Disparo>().machete)
        {
            txtMunicion.text = "";
        }
        else
        {
            txtMunicion.text = "Munici�n: " + jugador.GetComponent<Player_Disparo>().municionActual + "/" + jugador.GetComponent<Player_Disparo>().municion;
        }
        txtMunicionSecundaria.text = "x " + jugador.GetComponent<Player_Disparo>().municionSecundaria;
        if (GameManager.gameOver || GameManagerJefe.jefeGameOver || GameManagerJefe2.jefe2GameOver || GameManager_Lvl3.gameOverLvl3)
        {
            txtEstadoDelJuego.gameObject.SetActive(true);
        }
        else
        {
            txtEstadoDelJuego.gameObject.SetActive(false);
        }
    }
    public void ActualizarContadorMadera()
    {
        txtContadorMadera.text = "x " + jugador.GetComponent<Player_DestruirObstaculos>().cantMadera.ToString();
    }

}
