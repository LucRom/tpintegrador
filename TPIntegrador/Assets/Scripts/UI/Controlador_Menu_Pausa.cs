using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Controlador_Menu_Pausa : MonoBehaviour
{
    public bool menuDePausaAbierto;
    public bool lvl3;
    [SerializeField] GameObject menuDePausa;
    [SerializeField] GameObject jugador;
    [SerializeField] GameObject camioneta;

    private void Start()
    {
        if(lvl3 == false)
        {
            jugador = GameObject.Find("Jugador");
            camioneta = GameObject.Find("Camioneta");
        }
        menuDePausaAbierto = false;
    }
    public void Pausa(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            menuDePausaAbierto = !menuDePausaAbierto;
            CheckMenuPausa();
        }
    }
    private void CheckMenuPausa()
    {
        if (menuDePausaAbierto)
        {
            Time.timeScale = 0;
            if(jugador != null && lvl3 == false)
            {
                jugador.GetComponent<Player_Movimiento>().enabled = false;
                jugador.GetComponent<Player_Disparo>().enabled = false;
                jugador.GetComponent<Player_Cheats>().enabled = false;
            }
            if (jugador != null && lvl3)
            {
                camioneta.GetComponent<Camioneta_Movimiento_Lvl3>().enabled = false;
                jugador.GetComponent<Jugador_Disparlo_Lvl3>().enabled = false;
                jugador.GetComponent<Player_Cheats>().enabled = false;
            }
            menuDePausa.SetActive(true);
        }
        else
        {

            if (jugador != null && lvl3 == false)
            {
                jugador.GetComponent<Player_Movimiento>().enabled = true;
                jugador.GetComponent<Player_Disparo>().enabled = true;
                jugador.GetComponent<Player_Cheats>().enabled = true;
            }
            if (jugador != null && lvl3)
            {
                camioneta.GetComponent<Camioneta_Movimiento_Lvl3>().enabled = true;
                jugador.GetComponent<Jugador_Disparlo_Lvl3>().enabled = true;
                jugador.GetComponent<Player_Cheats>().enabled = true;
            }
            Time.timeScale = 1;
            menuDePausa.SetActive(false);

        }
    }
    public void CerrarMenuPausa()
    {
        menuDePausaAbierto = false;
        CheckMenuPausa();
    }
    public void MenuPrincipal()
    {
        if (jugador.activeInHierarchy && camioneta.activeInHierarchy && lvl3 == false)
        {
            ManagerStats.instancia.DevolverDatosDefault();
        }
        SceneManager.LoadScene("Menu Principal");
    }
}
