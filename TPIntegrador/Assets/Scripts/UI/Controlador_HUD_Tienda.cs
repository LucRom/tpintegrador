using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_HUD_Tienda : MonoBehaviour
{
    [Header("Textos")]
    [SerializeField] TMPro.TMP_Text dinero;
    [Header("GameObjects")]
    [SerializeField] GameObject player;
    private Player_Dinero playerDinero;
    void Start()
    {
        playerDinero = player.GetComponent<Player_Dinero>();
    }
    void Update()
    {
        dinero.text = playerDinero.dinero.ToString();
        if(playerDinero.dinero <= 0)
        {
            dinero.text = "0";
        }
    }
}
