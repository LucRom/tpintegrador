using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controlador_Menu_Principal : MonoBehaviour
{
    private void Start()
    {
        GestorDeAudio.instancia.PausarSonido("MacheteFuriaMusic");
    }
    public void Jugar()
    {
        SceneManager.LoadScene("Lvl1");
    }
    public void Salir()
    {
        Application.Quit();
    }
}
