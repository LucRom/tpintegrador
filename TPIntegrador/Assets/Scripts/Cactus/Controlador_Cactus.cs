using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Cactus : MonoBehaviour
{
    [SerializeField] float da�o;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDa�oJugador(da�o);
        }
    }
}
