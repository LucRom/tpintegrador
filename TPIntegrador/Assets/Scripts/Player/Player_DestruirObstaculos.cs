using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Player_DestruirObstaculos : MonoBehaviour
{
    [Header("Parametros")]
    [SerializeField] public float dañoAObstaculos;
    [SerializeField] float distancia;
    [SerializeField] bool tienePuente;
    public int cantMadera;
    public UnityEvent construirPuente;
    private void Start()
    {
        cantMadera = 0;
    }
    public void DestruirObstaculo(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, distancia))
            {
                if (hit.transform.gameObject.CompareTag("Obstaculo") && tienePuente == false)
                {
                    GestorDeAudio.instancia.ReproducirSonido("GolpeObstaculo");
                    hit.transform.gameObject.GetComponent<Controlador_Vida>().RecibirDañoDesaparecer(dañoAObstaculos);
                }
                if (hit.transform.gameObject.CompareTag("Construccion") && tienePuente)
                {
                    if (cantMadera == 3)
                    {
                        construirPuente.Invoke();
                    }
                }
            }
        }
    }
    public void SumarMadera()
    {
        cantMadera++;
    }
}
