using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class Player_Movimiento : MonoBehaviour
{
    private Rigidbody rb;
    [Header("Movimiento")]
    public float velocidad;
    public float velocidadMod;
    public float fuerzaDash;
    private bool estaDasheando;
    [SerializeField] float tiempoEfectoDash;
    private float timerDash;
    private PlayerInput playerInput;
    public bool keyboard;
    private Vector2 movimiento;
    public Vector2 mirarMouse, mirarJoystick;
    private Vector3 vrotacion;
    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
    }
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        keyboard = true;
        gameObject.GetComponent<TrailRenderer>().enabled = false;
        timerDash = tiempoEfectoDash;
        estaDasheando = false;
        SetVelocidadMod();
    }
    private void Update()
    {
        Movimiento();

        if(mirarJoystick.x != 0 && mirarJoystick.y != 0)
        {
            keyboard = false;
        }
        else
        {
            keyboard = true;
        }

        if (keyboard)
        {
            MirarMouse();
        }
        else
        {
            MirarJoystick();
        }
        if (estaDasheando)
        {
            EfectoDash();
        }
    }
    public void SetMovimiento(InputAction.CallbackContext context)
    {
        movimiento = context.ReadValue<Vector2>();
    }
    public void SetMirarMouse(InputAction.CallbackContext context)
    {
        mirarMouse = context.ReadValue<Vector2>();
    }
    public void SetMirarJoystick(InputAction.CallbackContext context)
    {
        mirarJoystick = context.ReadValue<Vector2>();
    }
    public void SetDash(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Dash();
        }
    }
    private void Dash()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position, transform.forward, out hit, 5))
        {
            estaDasheando = true;
            gameObject.GetComponent<TrailRenderer>().enabled = true;
            rb.AddForce(transform.forward * fuerzaDash, ForceMode.Impulse);
        }
    }
    private void EfectoDash()
    {
        timerDash -= Time.deltaTime;
        if(timerDash <= 0)
        {
            gameObject.GetComponent<TrailRenderer>().enabled = false;
            estaDasheando = false;
            timerDash = tiempoEfectoDash;
        }
    }
    private void Movimiento()
    {
        Vector3 vmovimiento = new Vector3(movimiento.x, 0, movimiento.y);
        transform.Translate(vmovimiento * velocidadMod * Time.deltaTime, Space.World);
    }
    private void MirarMouse()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(mirarMouse);

        if (Physics.Raycast(ray, out hit))
        {
            vrotacion = hit.point;
        }

        var posMirar = vrotacion - transform.position;
        posMirar.y = 0;
        if(posMirar != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(posMirar), 0.15f);
        }
    }
    private void MirarJoystick()
    {
        Vector3 direccion = new Vector3(mirarJoystick.x, 0, mirarJoystick.y);

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direccion), 0.15f);
    }
    public void SetVelocidadMod()
    {
        velocidadMod = velocidad;
    }

}
