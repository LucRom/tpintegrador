using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public class Player_Dinero : MonoBehaviour
{
    [SerializeField] public int dinero;
    private GameObject vendedor;
    private bool puedecomprar;
    private bool abrioMenuCompras;
    private void Start()
    {
        vendedor = null;
        abrioMenuCompras= false;
    }
    private void Update() 
    {
        if (abrioMenuCompras && vendedor != null)
        {
            vendedor.GetComponent<Controlador_Vendedor>().estaComprando = true;
            vendedor.GetComponent<Controlador_Vendedor>().SetJugador(gameObject);
            gameObject.GetComponent<Player_Movimiento>().enabled = false;
        }
        else
        {
            if (vendedor != null)
            {
                vendedor.GetComponent<Controlador_Vendedor>().estaComprando = false;
                vendedor.GetComponent<Controlador_Vendedor>().SetJugador(null);
                gameObject.GetComponent<Player_Movimiento>().enabled = true;
            }
        }
    }
    public void AbrirMenuCompra(InputAction.CallbackContext context)
    {
        if (context.performed && puedecomprar)
        {
            abrioMenuCompras = !abrioMenuCompras;
        }
    }
    public void SetVendedor(GameObject pVendedor)
    {
        puedecomprar = !puedecomprar;
        vendedor = pVendedor;
    }
}
