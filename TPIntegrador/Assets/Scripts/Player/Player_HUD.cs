using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_HUD : MonoBehaviour
{
    [Header("HitMarker/KillMarker")]
    [SerializeField] Image hitMarker;
    [SerializeField] float cooldownHitMarker;
    private float timerHitMarker;
    private bool hitmarker;
    [Header("ImagenRecarga")]
    [SerializeField] public Image recarga;
    void Start()
    {
        hitmarker = false;
        timerHitMarker = cooldownHitMarker;
        hitMarker.gameObject.SetActive(false);
        recarga.gameObject.SetActive(false);
    }
    void Update()
    {
        if (hitmarker)
        {
            TimerHitMarker();
        }
    }
    public void MostrarHitMarker()
    {
        hitMarker.color = Color.white;
        hitMarker.gameObject.SetActive(true);
        hitmarker = true;
    }
    public void MostrarHitMarkerKill()
    {
        hitMarker.color = Color.red;
        hitMarker.gameObject.SetActive(true);
        hitmarker = true;
    }
    private void TimerHitMarker()
    {
        timerHitMarker -= Time.deltaTime;
        if (timerHitMarker <= 0)
        {
            hitMarker.gameObject.SetActive(false);
            hitmarker = false;
            timerHitMarker = cooldownHitMarker;
        }
    }
}
