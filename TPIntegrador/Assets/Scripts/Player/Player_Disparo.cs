using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;
using UnityEngine.VFX;
using Unity.VisualScripting;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class Player_Disparo : MonoBehaviour
{
    [Header("Disparo")]
    [SerializeField] GameObject bala;
    [SerializeField] public GameObject arma;
    [SerializeField] public int municion;
    [SerializeField] public int municionActual;
    [SerializeField] public float da�o;
    [SerializeField] float cooldownRecarga;
    [SerializeField] public float cadencia;
    private float siguienteBala;
    private float timerRecarga;
    private bool disparar;
    private bool recargando;
    [Header("Disparo Secundario")]
    [SerializeField] GameObject balaSecundaria;
    [SerializeField] public int municionSecundaria;
    [SerializeField] public float multiplicadorDistancia;
    private float distanciaDisparoSecundario;
    private bool disparoSecundario;
    [Header("Escopeta")]
    [SerializeField] public GameObject armaEscopeta;
    [SerializeField] GameObject balaEscopeta;
    public bool escopeta;
    [SerializeField] public float municionEscopeta;
    [SerializeField] float da�oEscopeta;
    [SerializeField] float empujeEscopeta;
    [SerializeField] float cadenciaEscopeta;
    private float siguienteBalaEscopeta;
    [Header("Lanza Cohetes")]
    [SerializeField] public GameObject armaLanzaCohetes;
    [SerializeField] GameObject cohetes;
    public bool lanzaCohete;
    [SerializeField] public float municionLanzaCohete;
    [SerializeField] float da�oLanzaCohete;
    [SerializeField] float empujeLanzaCohete;
    [SerializeField] float cadenciaLanzaCohete;
    private float siguienteBalaLanzaCohete;
    [Header("Melee")]
    [SerializeField] public GameObject armaMelee;
    [SerializeField] public GameObject zonaMelee;
    public bool machete;
    [SerializeField] float da�oMele;
    [SerializeField] float cooldownMelee;
    private float timerMelee;
    [SerializeField] Animator ataqueMelee;
    [SerializeField] GameObject postProceso;
    private ColorAdjustments furia;
    void Start()
    {
        recargando = false;
        disparar = false;
        disparoSecundario = false;
        timerRecarga = cooldownRecarga;
        timerMelee = cooldownMelee;
        municionActual = municion;
        postProceso.GetComponent<Volume>().profile.TryGet(out furia);
        furia.active = false;
        
    }
    void Update()
    {
        if (disparar && recargando == false)
        {
            if(escopeta && municionEscopeta > 0)
            {
                DisparoEscopeta();
            }
            else if (lanzaCohete && municionLanzaCohete >0)
            {
                DisparoLanzaCohetes();
            }
            else if (machete)
            {
                AtaqueMelee();
            }
            else
            {
                Disparo();
            }
        }
        if(municionEscopeta <= 0 && escopeta)
        {
            armaEscopeta.SetActive(false);
            arma.SetActive(true);
            escopeta = false;
        }
        if (municionLanzaCohete <= 0 && lanzaCohete)
        {
            armaLanzaCohetes.SetActive(false);
            arma.SetActive(true);
            lanzaCohete = false;
        }
        if (municionActual <= 0)
        {
            recargando = true;
            Timer();
        }
        if (recargando)
        {
            if(gameObject.GetComponent<Player_HUD>().recarga.fillAmount == 1)
            {
                GestorDeAudio.instancia.ReproducirSonido("Recarga");
            }
            gameObject.GetComponent<Player_HUD>().recarga.fillAmount -= 1 / cooldownRecarga * Time.deltaTime;
        }
        if (disparoSecundario && distanciaDisparoSecundario <= 10 && municionSecundaria > 0)
        {
            gameObject.GetComponent<Player_Movimiento>().enabled = false;
            distanciaDisparoSecundario += Time.deltaTime * multiplicadorDistancia;
        }
        if (machete)
        {
            furia.active = true;
            timerMelee -= Time.deltaTime;
            GetComponent<Controlador_Vida>().vidaActual = GetComponent<Controlador_Vida>().vida;
            GetComponent<Controlador_Vida>().RellenarBarraDeVida();
            if (timerMelee <= 0)
            {
                PausarFuria();
                SetVida();
                arma.SetActive(true);
                armaMelee.SetActive(false);
                machete = false;
                ResetTimerMachete();
            }
        }
    }
    private void Timer()
    {
        gameObject.GetComponent<Player_HUD>().recarga.gameObject.SetActive(true);
        timerRecarga -= Time.deltaTime;
        if(timerRecarga <= 0)
        {
            municionActual = municion;
            recargando= false;
            gameObject.GetComponent<Player_HUD>().recarga.fillAmount = 1;
            gameObject.GetComponent<Player_HUD>().recarga.gameObject.SetActive(false);
            timerRecarga = cooldownRecarga;
        }
    }
    public void Disparar(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            disparar = true;
        }
        if(context.canceled)
        {
            disparar = false;
            if (machete)
            {
                zonaMelee.SetActive(false);
            }
        } 
    }
    public void Recargar(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            GestorDeAudio.instancia.ReproducirSonido("Recarga");
            municionActual = 0;
        }
    }
    public void DispararSecundario(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            disparoSecundario = true;
        }
        if (context.canceled)
        {
            DisparoSecundario();
            gameObject.GetComponent<Player_Movimiento>().enabled = true;
        }
    }
    private void Disparo()
    {
        if (Time.time > siguienteBala)
        {
            siguienteBala = Time.time + cadencia;
            municionActual--;
            GameObject nuevaBala = Instantiate(bala, arma.transform.position, arma.transform.rotation);
            nuevaBala.GetComponent<Controlador_Bala>().disparoJugador = true;
            nuevaBala.GetComponent<Controlador_Bala>().da�o = da�o;
        }
    }
    private void DisparoSecundario()
    {
        if(municionSecundaria > 0)
        {
            municionSecundaria--;
            disparoSecundario = false;
            GameObject nuevaBalaSecuundaria = Instantiate(balaSecundaria, arma.transform.position, transform.rotation);
            Rigidbody rbBalaSecundaria = nuevaBalaSecuundaria.GetComponent<Rigidbody>();
            rbBalaSecundaria.velocity = transform.forward * distanciaDisparoSecundario;
            distanciaDisparoSecundario = 0;
        }
    }
    private void DisparoEscopeta()
    {
        if (Time.time > siguienteBalaEscopeta)
        {
            siguienteBalaEscopeta = Time.time + cadenciaEscopeta;
            municionEscopeta--;
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.AddForce(-transform.forward * empujeEscopeta, ForceMode.Impulse);
            GameObject nuevaBala = Instantiate(balaEscopeta, armaEscopeta.transform.position, armaEscopeta.transform.rotation);
            nuevaBala.GetComponent<Controlador_BalaEscopeta>().disparoJugador = true;
            nuevaBala.GetComponent<Controlador_BalaEscopeta>().da�o = da�oEscopeta;
        }
    }
    private void DisparoLanzaCohetes()
    {
        if(Time.time > siguienteBalaLanzaCohete)
        {
            siguienteBalaLanzaCohete = Time.time + cadenciaLanzaCohete;
            municionLanzaCohete--;
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.AddForce(-transform.forward * empujeLanzaCohete, ForceMode.Impulse);
            GameObject nuevaBala = Instantiate(cohetes, armaLanzaCohetes.transform.position, armaLanzaCohetes.transform.rotation);
            nuevaBala.GetComponent<Controlador_Cohete>().disparoJugador = true;
            nuevaBala.GetComponent<Controlador_Cohete>().da�o = da�oLanzaCohete;
        }
    }
    private void AtaqueMelee()
    {
        zonaMelee.SetActive(true);
        ataqueMelee.SetBool("EstaAtacando", true);
        zonaMelee.GetComponent<Controlador_Machete>().da�o = da�oMele;
    }
    public void PausarFuria()
    {
        GestorDeAudio.instancia.PausarSonido("MacheteFuriaMusic");
        GetComponent<Controlador_Vida>().MostrarBarraDeVida();
        furia.active = false;
    }
    private void SetVida()
    {
        GetComponent<Controlador_Vida>().vidaActual = GetComponent<Controlador_Vida>().vida;
        GetComponent<Controlador_Vida>().MostrarBarraDeVida();
        GetComponent<Controlador_Vida>().RellenarBarraDeVida();
    }
    public void ResetTimerMachete()
    {
        timerMelee = cooldownMelee;
    }
}
