using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Player_Cheats : MonoBehaviour
{
    public void SumarPlata(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            GetComponent<Player_Dinero>().dinero += 1000;
        }
    }
    public void RestaurarVida(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            GetComponent<Controlador_Vida>().vidaActual = GetComponent<Controlador_Vida>().vida;
            GetComponent<Controlador_Vida>().RellenarBarraDeVida();
        }
    }
    public void RestaurarVidaCamioneta(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            GameObject camioneta = GameObject.Find("Camioneta");
            camioneta.GetComponent<Controlador_Vida>().vidaActual = camioneta.GetComponent<Controlador_Vida>().vida;
            camioneta.GetComponent<Controlador_Vida>().RellenarBarraDeVida();
        }
    }
    public void IrAlFinal(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            GameObject camioneta = GameObject.Find("Camioneta");
            camioneta.transform.position = new Vector3(62.25f, 2.5f, 0);
            transform.position = new Vector3(62.25f, 2.5f, -3);
        }
    }
    public void IrTienda(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            ManagerStats.instancia.GuardarDatos();
            SceneManager.LoadScene("Tienda");
        }
    }
    public void IrJefe(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            ManagerStats.instancia.GuardarDatos();
            SceneManager.LoadScene("Jefe");
        }
    }
    public void IrJefe2(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            ManagerStats.instancia.GuardarDatos();
            SceneManager.LoadScene("Jefe 2");
        }
    }
    public void IrLvl1(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            ManagerStats.instancia.GuardarDatos();
            SceneManager.LoadScene("Lvl1");
        }
    }
    public void IrLvl2(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            ManagerStats.instancia.GuardarDatos();
            SceneManager.LoadScene("Lvl2");
        }
    }
    public void IrLvl3(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            ManagerStats.instancia.GuardarDatos();
            SceneManager.LoadScene("Lvl3");
        }
    }
}
