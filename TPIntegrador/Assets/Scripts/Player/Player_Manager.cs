using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player_Manager : MonoBehaviour
{
    [SerializeField] List<PlayerInput> jugadores = new List<PlayerInput>();
    [SerializeField] GameObject spawnPoint;

    public void NuevoJugador(PlayerInput jugador) 
    {
        jugadores.Add(jugador);
        jugador.transform.position = spawnPoint.transform.position;
    }
}
