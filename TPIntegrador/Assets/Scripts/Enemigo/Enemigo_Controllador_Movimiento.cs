using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemigo_Controllador_Movimiento : MonoBehaviour
{
    private GameObject camioneta;
    public GameObject jugadorABuscar;
    private NavMeshAgent agent;
    public bool soloBuscarAlJugador;
    public bool detectoJugador;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        camioneta = GameObject.Find("Camioneta");
        detectoJugador = false;
        if(soloBuscarAlJugador)
        {
            jugadorABuscar = GameObject.Find("Jugador");
        }
    }
    void Update()
    {
        if(soloBuscarAlJugador == false)
        {
            if (detectoJugador && jugadorABuscar != null)
            {
                agent.SetDestination(jugadorABuscar.transform.position);
            }
            else if (camioneta != null)
            {
                transform.LookAt(camioneta.transform.position);
                agent.SetDestination(camioneta.transform.position);
            }
        }
        else
        {
            if(jugadorABuscar != null)
            {
                agent.SetDestination(jugadorABuscar.transform.position);
            }
        }
    }
}
