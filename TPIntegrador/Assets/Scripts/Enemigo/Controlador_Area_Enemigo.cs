using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Area_Enemigo : MonoBehaviour
{
    public GameObject Enemigo;
    private Enemigo_Controllador_Movimiento movimiento;

    private void Start()
    {
        movimiento = Enemigo.GetComponent<Enemigo_Controllador_Movimiento>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            movimiento.jugadorABuscar = other.gameObject;
            movimiento.detectoJugador = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            movimiento.detectoJugador = false;
        }
    }
}
