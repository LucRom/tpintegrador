using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Camioneta_Movimiento : MonoBehaviour
{
    [Header("Propiedades")]
    [SerializeField] public float velocidad;
    [SerializeField] float velocidadMod;
    [SerializeField] public float boostVelocidad;
    [SerializeField] public float distancia;
    [SerializeField] public float distanciaRecord;
    [SerializeField] float boostdistancia;
    [SerializeField] float distanciaDeteccionObstaculos;
    private float velocidadaux;
    public bool segundaOportunidad;
    private bool activarDetectarObstaculo;
    private void Start()
    {
        SetVelocidadMod();
        activarDetectarObstaculo = true;
    }
    void Update()
    {
        transform.position += transform.right * velocidadMod * Time.deltaTime;
        distancia += Time.deltaTime;
        if(distancia > distanciaRecord)
        {
            distanciaRecord = distancia;
        }
        DetectarObstaculos();
        if(velocidadMod <=0)
        {
            velocidadMod = 1;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstaculo") && segundaOportunidad == false)
        {
            ManagerStats.instancia.DevolverDatosDefault();
            gameObject.SetActive(false);
            GameManager.gameOver = true;
        }
        else if(collision.gameObject.CompareTag("Obstaculo") && segundaOportunidad)
        {
            Destroy(collision.gameObject);
            segundaOportunidad= false;
        }
    }
    public void SumarVelocidad()
    {
        velocidadMod += boostVelocidad;
        distancia += boostdistancia;
    }
    public void VolverALaVelocidad()
    {
        velocidadMod -= boostVelocidad;
        distancia -= boostdistancia;
        distanciaRecord -= boostdistancia;
    }
    public void SetVelocidadMod()
    {
        velocidadaux = velocidad;
        velocidadMod = velocidad;
    }
    private void DetectarObstaculos()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position, transform.right, out hit, distanciaDeteccionObstaculos) && activarDetectarObstaculo)
        {
            if (hit.transform.CompareTag("Obstaculo"))
            {
                velocidadMod = 1;
            }
            else
            {
                velocidadMod = velocidadaux;
                activarDetectarObstaculo = false;
            }
        }
    }
}
