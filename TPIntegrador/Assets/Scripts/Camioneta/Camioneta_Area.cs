using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Camioneta_Area : MonoBehaviour
{
    public UnityEvent JugadorEnArea;
    public UnityEvent JugadorFueraDeArea;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            JugadorEnArea.Invoke();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            JugadorFueraDeArea.Invoke();
        }
    }
}
