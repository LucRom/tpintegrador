using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_HUD_Lvl3 : MonoBehaviour
{
    [Header("Textos")]
    [SerializeField] TMPro.TMP_Text txtDinero;
    [SerializeField] TMPro.TMP_Text txtEstadoDelJuego;
    [Header("Extras")]
    [SerializeField] GameObject jugador;
    void Update()
    {
        txtDinero.text = "$ " + jugador.GetComponent<Player_Dinero>().dinero.ToString();
        if (GameManager.gameOver || GameManagerJefe.jefeGameOver || GameManagerJefe2.jefe2GameOver || GameManager_Lvl3.gameOverLvl3)
        {
            txtEstadoDelJuego.gameObject.SetActive(true);
        }
        else
        {
            txtEstadoDelJuego.gameObject.SetActive(false);
        }
    }
}
