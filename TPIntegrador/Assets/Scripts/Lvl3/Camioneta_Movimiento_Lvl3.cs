using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Camioneta_Movimiento_Lvl3 : MonoBehaviour
{
    private int posicion;
    private void Start()
    {
        posicion = 0;
    }
    public void MovimientoDerecha(InputAction.CallbackContext context)
    {
        if (context.performed && posicion != 3)
        {
            posicion++;
            ActualizarPosicion();
        }
    }
    public void MovimientoIzquierda(InputAction.CallbackContext context)
    {
        if (context.performed && posicion != 0)
        {
            posicion--;
            ActualizarPosicion();
        }
    }
    private void ActualizarPosicion()
    {
        switch (posicion)
        {
            case 0:
                transform.position = new Vector3(-6, 2.25f, -6);
                break;
            case 1:
                transform.position = new Vector3(-2.25f, 2.25f, -6);
                break;
            case 2:
                transform.position = new Vector3(2.25f, 2.25f, -6);
                break;
            case 3:
                transform.position = new Vector3(6, 2.25f, -6);
                break;
        }
    }
}
