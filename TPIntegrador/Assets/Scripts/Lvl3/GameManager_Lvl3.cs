using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager_Lvl3 : MonoBehaviour
{
    [Header("GameObjects")]
    [SerializeField] GameObject jugador;
    [SerializeField] GameObject barreras;
    private Controlador_Vida vidaJugador;
    [Header("Listas")]
    [SerializeField] List<GameObject> listaobjetosSpawn = new List<GameObject>();
    [SerializeField] List<Transform> listaSpawmsBarreras = new List<Transform>();
    [SerializeField] List<Transform> listaSpawmsEnemigos = new List<Transform>();
    [SerializeField] List<GameObject> listaEnemigos = new List<GameObject>();
    [Header("Timers")]
    [SerializeField] float cooldownTiempo;
    [SerializeField] float cooldownBarreras;
    [SerializeField] float cooldownEnemigo;
    private float timer;
    private float timerBarrera;
    private float timerEnemigo;
    public static bool gameOverLvl3;
    [SerializeField] Image barraDeTiempo;
    [SerializeField] Image barraDeTiempoFondo;
    void Start()
    {
        Comenzar();
    }
    void Update()
    {
        if(vidaJugador.vidaActual <= 0)
        {
            gameOverLvl3= true;
        }
        if(gameOverLvl3)
        {
            Time.timeScale = 0;
        }
        GenerarBarreras();
        GenerarEnemigos();
        Tiempo();
    }
    private void Comenzar()
    {
        ManagerStats.instancia.DevolverDatos();
        gameOverLvl3 = false;
        vidaJugador = jugador.GetComponent<Controlador_Vida>();
        timerBarrera= cooldownBarreras;
        timerEnemigo = cooldownEnemigo;
        timer = cooldownTiempo;
    }
    public void GenerarBarreras()
    {
        timerBarrera -= Time.deltaTime;
        if(timerBarrera <=0)
        {
            Instantiate(listaobjetosSpawn[UnityEngine.Random.Range(0, listaobjetosSpawn.Count)], listaSpawmsBarreras[UnityEngine.Random.Range(0, listaSpawmsBarreras.Count)]);
            Instantiate(barreras, listaSpawmsBarreras[UnityEngine.Random.Range(0, listaSpawmsBarreras.Count)]);
            timerBarrera = UnityEngine.Random.Range(0, cooldownBarreras);
        }
    }
    public void GenerarEnemigos()
    {
        timerEnemigo -= Time.deltaTime;
        if (timerEnemigo <= 0)
        {
            Instantiate(listaEnemigos[UnityEngine.Random.Range(0, listaEnemigos.Count)], listaSpawmsEnemigos[UnityEngine.Random.Range(0, listaSpawmsBarreras.Count)]);
            timerEnemigo = UnityEngine.Random.Range(0, cooldownBarreras);
        }
    }
    private void Tiempo()
    {
        barraDeTiempo.fillAmount = timer / cooldownTiempo;
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            ManagerStats.instancia.GuardarDatos();
            SceneManager.LoadScene("Tienda");
        }
    }
}
