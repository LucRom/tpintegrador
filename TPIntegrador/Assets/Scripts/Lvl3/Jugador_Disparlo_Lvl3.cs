using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Jugador_Disparlo_Lvl3 : MonoBehaviour
{
    [Header("Apuntado")]
    public bool keyboard;
    public Vector2 mirarMouse, mirarJoystick;
    private Vector3 vrotacion;
    [Header("Disparo")]
    [SerializeField] GameObject torreta;
    [SerializeField] GameObject bala;
    [SerializeField] float da�o;
    [SerializeField] float segsParaDisparar;
    [SerializeField] float cooldownTorreta;
    [SerializeField] float calentamientoTorreta;
    private float timerTorreta;
    private bool disparo;
    private bool puedeDisparar;
    private bool inicioTimerEnfriamiento;
    [SerializeField] Image barraDeCalentamiento;
    [SerializeField] Image barraDeCalentamientoFondo;
    private void Start()
    {
        puedeDisparar = true;
        inicioTimerEnfriamiento= false;
        timerTorreta = cooldownTorreta;
    }
    void Update()
    {
        if (mirarJoystick.x != 0 && mirarJoystick.y != 0)
        {
            keyboard = false;
        }
        else
        {
            keyboard = true;
        }

        if (keyboard)
        {
            MirarMouse();
        }
        else
        {
            MirarJoystick();
        }

        if(disparo)
        {
            Disparo();
            Calentar();
        }
        else
        {
            Enfriar();
        }
        if (calentamientoTorreta >= segsParaDisparar)
        {
            inicioTimerEnfriamiento= true;
            disparo= false;
            puedeDisparar = false;
        }
        if(inicioTimerEnfriamiento)
        {
            timerTorreta -= Time.deltaTime;
            if(timerTorreta <= 0)
            {
                calentamientoTorreta= 0;
                puedeDisparar = true;
                inicioTimerEnfriamiento = false;
                timerTorreta = cooldownTorreta;
            }
        }
    }
    public void SetMirarMouse(InputAction.CallbackContext context)
    {
        mirarMouse = context.ReadValue<Vector2>();
    }
    public void SetMirarJoystick(InputAction.CallbackContext context)
    {
        mirarJoystick = context.ReadValue<Vector2>();
    }
    public void Disparar(InputAction.CallbackContext context)
    {
        if (context.performed && puedeDisparar)
        {
            disparo = true;
        }
        if(context.canceled)
        {
            disparo = false;
        }
    }
    private void MirarMouse()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(mirarMouse);

        if (Physics.Raycast(ray, out hit))
        {
            vrotacion = hit.point;
        }

        var posMirar = vrotacion - transform.position;
        posMirar.y = 0;
        if (posMirar != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(posMirar), 0.15f);
        }
    }
    private void MirarJoystick()
    {
        Vector3 direccion = new Vector3(mirarJoystick.x, 0, mirarJoystick.y);

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direccion), 0.15f);
    }
    private void Disparo()
    {
        GameObject nuevaBala = Instantiate(bala, torreta.transform.position, torreta.transform.rotation);
        nuevaBala.GetComponent<Controlador_Bala_Torreta>().da�o = da�o;
    }
    private void Calentar()
    {
        if(calentamientoTorreta <= 6)
        {
            calentamientoTorreta += Time.deltaTime;
            barraDeCalentamiento.color = Color.red;
            barraDeCalentamiento.fillAmount = calentamientoTorreta / segsParaDisparar;
        }
    }
    private void Enfriar()
    {
        if(calentamientoTorreta >= 0 && inicioTimerEnfriamiento == false)
        {
            calentamientoTorreta -= Time.deltaTime;
            barraDeCalentamiento.color= Color.blue;
            barraDeCalentamiento.fillAmount = calentamientoTorreta / segsParaDisparar;
        }
        
    }
}
