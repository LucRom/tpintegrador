using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Barreras : MonoBehaviour
{
    [SerializeField] float velocidad;
    public float da�o;
    void Update()
    {
        transform.Translate(-transform.forward * velocidad * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Limites"))
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDa�oJugador(da�o);
            Destroy(gameObject);
        }
    }
}
