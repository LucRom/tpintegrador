using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemigos_BuscarCamioneta : MonoBehaviour
{
    public GameObject jugadorABuscar;
    private NavMeshAgent agent;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        jugadorABuscar = GameObject.Find("Pivote camioneta/jugador");
    }
    void Update()
    {
        agent.SetDestination(jugadorABuscar.transform.position);
        transform.rotation = Quaternion.Euler(0,0,0);
    }
}
