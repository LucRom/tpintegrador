using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_Disparo : MonoBehaviour
{
    [Header("Propiedades")]
    [SerializeField] public float daņoJugador;
    public bool dispara;
    [Header("Disparo")]
    [SerializeField] GameObject objetivo;
    [SerializeField] GameObject arma;
    [SerializeField] GameObject bala;
    [SerializeField] float cooldownDisparo;
    private float timerDisparo;
    void Start()
    {
        objetivo = GameObject.Find("Pivote camioneta/jugador");
        timerDisparo = cooldownDisparo;
    }
    void Update()
    {
        transform.LookAt(objetivo.transform);
        Timer();
    }

    private void Timer()
    {
        timerDisparo -= Time.deltaTime;
        if (timerDisparo <= 0)
        {
            if (dispara)
            {
                RaycastHit hit;
                if (Physics.Raycast(arma.transform.position, transform.forward, out hit))
                {
                    if (hit.transform.CompareTag("Player"))
                    {
                        hit.transform.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoJugador(daņoJugador);
                    }
                }
            }
            Instantiate(bala, arma.transform.position, transform.rotation);
            timerDisparo = cooldownDisparo;
        }
    }
}

