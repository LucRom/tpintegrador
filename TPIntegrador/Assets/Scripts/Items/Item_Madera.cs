using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Item_Madera : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Player_DestruirObstaculos>().SumarMadera();
            GameObject.Find("Hud Manager").GetComponent<Controlador_HUD>().ActualizarContadorMadera();
            Destroy(this.gameObject);
        }
    }
}
