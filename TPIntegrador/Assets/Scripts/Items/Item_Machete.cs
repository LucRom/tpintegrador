using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Machete : MonoBehaviour
{
    [SerializeField] bool permanente;
    private float timer;
    [SerializeField] private float tiempoVida;
    private void Start()
    {
        timer = tiempoVida;
    }
    private void Update()
    {
        if (permanente == false)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                Destroy(gameObject);
            }
        }
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GestorDeAudio.instancia.ReproducirSonido("AgarrarMacheteSfx");
            GestorDeAudio.instancia.ReproducirSonido("MacheteFuriaMusic");
            other.gameObject.GetComponent<Player_Disparo>().escopeta = false;
            other.gameObject.GetComponent<Player_Disparo>().municionEscopeta = 0;
            other.gameObject.GetComponent<Player_Disparo>().arma.SetActive(false); 
            other.gameObject.GetComponent<Player_Disparo>().armaEscopeta.SetActive(false);
            other.gameObject.GetComponent<Player_Disparo>().lanzaCohete = false;
            other.gameObject.GetComponent<Player_Disparo>().armaLanzaCohetes.SetActive(false);
            other.gameObject.GetComponent<Player_Disparo>().municionLanzaCohete = 0;
            other.gameObject.GetComponent<Player_Disparo>().machete = true;
            other.gameObject.GetComponent<Player_Disparo>().armaMelee.SetActive(true);
            other.gameObject.GetComponent<Player_Disparo>().zonaMelee.SetActive(false);
            other.gameObject.GetComponent<Player_Disparo>().ResetTimerMachete();
            other.gameObject.GetComponent<Controlador_Vida>().OcultarBarraDeVida();
            Destroy(gameObject);
        }
    }
}
