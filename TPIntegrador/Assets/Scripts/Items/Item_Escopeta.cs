using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Escopeta : MonoBehaviour
{
    private float timer;
    [SerializeField] private float tiempoVida;
    private void Start()
    {
        timer = tiempoVida;
    }
    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Destroy(gameObject);
        }
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GestorDeAudio.instancia.ReproducirSonido("CargarEscopeta");
            other.gameObject.GetComponent<Player_Disparo>().lanzaCohete = false;
            other.gameObject.GetComponent<Player_Disparo>().municionLanzaCohete = 0;
            other.gameObject.GetComponent<Player_Disparo>().arma.SetActive(false);
            other.gameObject.GetComponent<Player_Disparo>().armaLanzaCohetes.SetActive(false);
            other.gameObject.GetComponent<Player_Disparo>().machete = false;
            other.gameObject.GetComponent<Player_Disparo>().armaMelee.SetActive(false);
            other.gameObject.GetComponent<Player_Disparo>().escopeta = true;
            other.gameObject.GetComponent<Player_Disparo>().armaEscopeta.SetActive(true);
            other.gameObject.GetComponent<Player_Disparo>().municionEscopeta = 10;
            other.gameObject.GetComponent<Player_Disparo>().PausarFuria();
            Destroy(gameObject);
        }
    }
}
