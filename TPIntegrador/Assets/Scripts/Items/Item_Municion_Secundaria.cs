using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Item_Municion_Secundaria : MonoBehaviour
{
    private float timer;
    [SerializeField]private float tiempoVida;
    private void Start()
    {
        timer = tiempoVida;
    }
    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Destroy(gameObject);
        }
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Municion");
            other.gameObject.GetComponent<Player_Disparo>().municionSecundaria++;
            Destroy(gameObject);
        }
    }
}
