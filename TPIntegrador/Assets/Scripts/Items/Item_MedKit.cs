using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Item_MedKit : MonoBehaviour
{
    private float timer;
    [SerializeField] private float tiempoVida;
    private void Start()
    {
        timer = tiempoVida;
    }
    private void Update()
    {
        timer-=Time.deltaTime;
        if(timer <= 0)
        {
            Destroy(gameObject);
        }
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Controlador_Vida>().vidaActual = other.gameObject.GetComponent<Controlador_Vida>().vida;
            other.gameObject.GetComponent<Controlador_Vida>().RellenarBarraDeVida();
            GestorDeAudio.instancia.ReproducirSonido("Vida");
            Destroy(gameObject);
        }
    }
}
