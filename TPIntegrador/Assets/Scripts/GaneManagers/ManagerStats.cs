using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerStats : MonoBehaviour
{
    public static ManagerStats instancia;
    public bool primeraPartida;
    [SerializeField] GameObject jugador;
    [SerializeField] GameObject camioneta;
    private float jugadorVida;
    private float jugadorVelocidad;
    private float jugadorDañoAObstaculos;
    private float jugadorCadencia;
    private int jugadorMunicion;
    private int jugadorDinero;
    private float camionetaVida;
    private float camionetaVelocidad;
    private bool camionetaSegundaOportunidad;
    private float camionetaDistancia;
    private float camionetaDistanciaRecord;

    private GameObject MenuCompraVendedor;
    private GameObject MenuCompraMecanico;
    private bool maxJugadorVida;
    private bool maxJugadorVelocidad;
    private bool maxJugadorMejoraArma;
    private int lvlJugadorVida = 1;
    private int lvlJugadorVelocidad = 1;
    private int lvlJugadorMejoraArma = 1;
    public int precioJugadorVida;
    private int precioJugadorVidaaux;
    public int precioJugadorVelocidad;
    private int precioJugadorVelocidadaux;
    public int precioJugadorMejoraArma;
    private int precioJugadorMejoraArmaaux;
    private bool maxCamionetaVida;
    private bool maxCamionetaVelocidad;
    private int lvlCamionetaResitsencia = 1;
    private int lvlCamionetaVelocidad = 1;
    public int precioCamionetaResistencia;
    private int precioCamionetaResistenciaaux;
    public int precioCamionetaVelocidad;
    private int precioCamionetaVelocidadaux;

    public static float nivelesParaElJefe = 1;
    public static float nivelDeDificutlad = 1;
    public static bool nivelEspecial = false;
    public static float vidaJefe = 500;

    public static int nivel2Jefe = 1;
    private void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        precioJugadorVidaaux = precioJugadorVida;
        precioJugadorVelocidadaux = precioJugadorVelocidad;
        precioJugadorMejoraArmaaux = precioJugadorMejoraArma;
        precioCamionetaResistenciaaux = precioCamionetaResistencia;
        precioCamionetaVelocidadaux = precioCamionetaVelocidad;
    }
    public void GuardarDatos()
    {
        jugador = GameObject.Find("Jugador");
        camioneta = GameObject.Find("Camioneta");
        jugadorVida = jugador.GetComponent<Controlador_Vida>().vida;
        jugadorVelocidad = jugador.GetComponent<Player_Movimiento>().velocidad;
        jugadorDañoAObstaculos = jugador.GetComponent<Player_DestruirObstaculos>().dañoAObstaculos;
        jugadorCadencia = jugador.GetComponent<Player_Disparo>().cadencia;
        jugadorMunicion = jugador.GetComponent<Player_Disparo>().municion;
        jugadorDinero = jugador.GetComponent<Player_Dinero>().dinero;
        camionetaVelocidad = camioneta.GetComponent<Camioneta_Movimiento>().velocidad;
        camionetaVida = camioneta.GetComponent<Controlador_Vida>().vida;
        camionetaSegundaOportunidad = camioneta.GetComponent<Camioneta_Movimiento>().segundaOportunidad;
        camionetaDistancia = camioneta.GetComponent<Camioneta_Movimiento>().distancia;
        camionetaDistanciaRecord = camioneta.GetComponent<Camioneta_Movimiento>().distanciaRecord;
        Debug.Log("Guardado");
    }
    public void DevolverDatos()
    {
        jugador = GameObject.Find("Jugador");
        camioneta = GameObject.Find("Camioneta");
        jugador.GetComponent<Controlador_Vida>().vida = jugadorVida;
        jugador.GetComponent<Player_Movimiento>().velocidad= jugadorVelocidad;
        jugador.GetComponent<Player_DestruirObstaculos>().dañoAObstaculos = jugadorDañoAObstaculos;
        jugador.GetComponent<Player_Disparo>().cadencia = jugadorCadencia;
        jugador.GetComponent<Player_Disparo>().municion = jugadorMunicion;
        jugador.GetComponent<Player_Dinero>().dinero = jugadorDinero;
        jugador.GetComponent<Controlador_Vida>().vidaActual = jugador.GetComponent<Controlador_Vida>().vida;
        jugador.GetComponent<Player_Movimiento>().SetVelocidadMod();
        camioneta.GetComponent<Camioneta_Movimiento>().velocidad = camionetaVelocidad;
        camioneta.GetComponent<Controlador_Vida>().vida = camionetaVida;
        camioneta.GetComponent<Controlador_Vida>().vidaActual = camioneta.GetComponent<Controlador_Vida>().vida;
        camioneta.GetComponent<Camioneta_Movimiento>().segundaOportunidad = camionetaSegundaOportunidad;
        camioneta.GetComponent<Camioneta_Movimiento>().distancia = camionetaDistancia;
        camioneta.GetComponent<Camioneta_Movimiento>().distanciaRecord = camionetaDistanciaRecord;
        camioneta.GetComponent<Camioneta_Movimiento>().SetVelocidadMod();
        Debug.Log("Cargado");
    }
    public void DevolverDatosDefault()
    {
        jugador = GameObject.Find("Jugador");
        camioneta = GameObject.Find("Camioneta");
        jugador.GetComponent<Controlador_Vida>().vida = 100;
        jugador.GetComponent<Player_Movimiento>().velocidad = 7;
        jugador.GetComponent<Player_DestruirObstaculos>().dañoAObstaculos = 25;
        jugador.GetComponent<Player_Disparo>().cadencia = 0.5f;
        jugador.GetComponent<Player_Disparo>().municion = 10;
        jugador.GetComponent<Player_Dinero>().dinero = 0;
        jugador.GetComponent<Controlador_Vida>().vidaActual = jugador.GetComponent<Controlador_Vida>().vida;
        jugador.GetComponent<Player_Movimiento>().SetVelocidadMod();
        camioneta.GetComponent<Camioneta_Movimiento>().velocidad = 1;
        camioneta.GetComponent<Controlador_Vida>().vida = 500;
        camioneta.GetComponent<Controlador_Vida>().vidaActual = camioneta.GetComponent<Controlador_Vida>().vida;
        camioneta.GetComponent<Camioneta_Movimiento>().segundaOportunidad = false;
        camioneta.GetComponent<Camioneta_Movimiento>().distancia = 0;
        camioneta.GetComponent<Camioneta_Movimiento>().SetVelocidadMod();
        lvlCamionetaResitsencia = 1;
        lvlCamionetaVelocidad = 1;
        maxCamionetaVida = false;
        maxCamionetaVelocidad = false;
        lvlJugadorVida = 1;
        lvlJugadorVelocidad = 1;
        lvlJugadorMejoraArma = 1;
        maxJugadorVida = false;
        maxJugadorVelocidad = false;
        maxJugadorMejoraArma = false;
        nivelDeDificutlad = 1;
        nivelesParaElJefe = 1;
        precioJugadorVida = precioJugadorVidaaux;
        precioJugadorVelocidad = precioJugadorVelocidadaux;
        precioJugadorMejoraArma = precioJugadorMejoraArmaaux;
        precioCamionetaResistencia = precioCamionetaResistenciaaux;
        precioCamionetaVelocidad = precioCamionetaVelocidadaux;
        primeraPartida = true;
        vidaJefe = 500;
        nivel2Jefe = 1;
        Debug.Log("Cargado Default");
    }
    public void GuardarDatosTiendaVendedor()
    {
        MenuCompraVendedor = GameObject.Find("MenuCompraVendedor");
        lvlJugadorVida = MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlVida;
        lvlJugadorVelocidad = MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlVelocidad;
        lvlJugadorMejoraArma = MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlFuerza;
        maxJugadorVida = MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlMaxVida;
        maxJugadorVelocidad = MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlMaxVelocidad;
        maxJugadorMejoraArma = MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlMaxFuerza;
        precioJugadorVida = MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().precioVida;
        precioJugadorVelocidad = MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().precioVelocidad;
        precioJugadorMejoraArma = MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().precioFuerza;
        Debug.Log("Guardado Tienda Vendedor");
    }
    public void GuardarDatosTiendaMecanico()
    {
        MenuCompraMecanico = GameObject.Find("MenuCompraMecanico");
        maxCamionetaVida = MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().lvlMaxResistencia;
        maxCamionetaVelocidad = MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().lvlMaxVelocidad;
        lvlCamionetaResitsencia = MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().lvlResistencia;
        lvlCamionetaVelocidad = MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().lvlVelocidad;
        precioCamionetaResistencia = MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().precioResistencia;
        precioCamionetaVelocidad = MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().precioVelocidad;
        Debug.Log("Guardado Tienda Mecanico");
    }
    public void DevolverDatosTiendaVendedor()
    {
        MenuCompraVendedor = GameObject.Find("MenuCompraVendedor");
        MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlMaxVida = maxJugadorVida;
        MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlMaxVelocidad = maxJugadorVelocidad;
        MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlMaxFuerza = maxJugadorMejoraArma;
        MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlVida = lvlJugadorVida;
        MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlVelocidad = lvlJugadorVelocidad;
        MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().lvlFuerza = lvlJugadorMejoraArma;
        MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().precioVida = precioJugadorVida;
        MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().precioVelocidad = precioJugadorVelocidad;
        MenuCompraVendedor.GetComponent<Controlador_Menu_Vendedor>().precioFuerza = precioJugadorMejoraArma;
        Debug.Log("Cargado Tienda Vendedor");
    }
    public void DevolverDatosTiendaMecanico()
    {
        MenuCompraMecanico = GameObject.Find("MenuCompraMecanico");
        MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().lvlMaxResistencia = maxCamionetaVida;
        MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().lvlMaxVelocidad = maxCamionetaVelocidad;
        MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().lvlResistencia = lvlCamionetaResitsencia;
        MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().lvlVelocidad = lvlCamionetaVelocidad;
        MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().precioResistencia = precioCamionetaResistencia;
        MenuCompraMecanico.GetComponent<Controlador_Menu_Mecanico>().precioVelocidad = precioCamionetaVelocidad;
        Debug.Log("Cargado Tienda Mecanico");
    }
    public void CheckNivelEspecial()
    {
        if(nivelesParaElJefe == 4)
        {
            nivelEspecial = true;
            Debug.Log("JEFE");
            nivelesParaElJefe = 0;
        }
    }
    public float AumentarVidaJefe()
    {
        vidaJefe += 500;
        return vidaJefe;
    }
    public void SumarDificultad2Jefe()
    {
        nivel2Jefe++;
    }

}
