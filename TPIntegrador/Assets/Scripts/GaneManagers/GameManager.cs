using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    [Header("GameObjects")]
    public GameObject jugador;
    public GameObject camioneta;
    [SerializeField] GameObject bandido;
    [SerializeField] GameObject zombie;
    [SerializeField] GameObject zombieRapido;
    [SerializeField] GameObject bandidoPesado;
    [SerializeField] GameObject jugadorSpawner;
    [SerializeField] GameObject camionetaSpawner;
    [SerializeField] List<GameObject> spawnersEnemigos = new List<GameObject>();
    [SerializeField] bool tienePuente;
    [SerializeField] GameObject obstaculo;
    [SerializeField] UnityEvent resetPuente;
    private List <GameObject> maderas= new List<GameObject>();
    private List<GameObject> listacactus = new List<GameObject>();
    public List<Transform> spawnObstaculos = new List<Transform>();
    private List<GameObject> obstaculos = new List<GameObject>();
    public static bool gameOver;
    [SerializeField] bool tieneCactus;
    [SerializeField] GameObject cactus;
    [Header("Parametros de dificultad")]
    #region Parametros Nivel Facil
    [Header("Nivel facil")]
    [Header("Bandidos")]
    [SerializeField] float bandidoTiempoParaSpawnearFacil;
    [SerializeField] float bandidoFacilDañoCamioneta;
    [SerializeField] float bandidoFacilDañoJugador;
    [SerializeField] float bandidoFacilVidaTotal;
    [Header("Bandidos Pesado")]
    [SerializeField] float bandidoPesadoTiempoParaSpawnearFacil;
    [SerializeField] float bandidoPesadoFacilDañoCamioneta;
    [SerializeField] float bandidoPesadoFacilDañoJugador;
    [SerializeField] float bandidoPesadoFacilVidaTotal;
    [Header("Zombie")]
    [SerializeField] float zombieTiempoParaSpawnearFacil;
    [SerializeField] float zombieFacilDañoCamioneta;
    [SerializeField] float zombieFacilDañoJugador;
    [SerializeField] float zombieFacilVidaTotal;
    [Header("ZombieRapido")]
    [SerializeField] float zombieRapidoTiempoParaSpawnearFacil;
    [SerializeField] float zombieRapidoFacilDañoCamioneta;
    [SerializeField] float zombieRapidoFacilDañoJugador;
    [SerializeField] float zombieRapidoFacilVidaTotal;
    #endregion
    #region Parametros Nivel Medio
    [Header("Nivel Medio")]
    [Header("Bandidos")]
    [SerializeField] float bandidoTiempoParaSpawnearMedio;
    [SerializeField] float bandidoMedioDañoCamioneta;
    [SerializeField] float bandidoMedioDañoJugador;
    [SerializeField] float bandidoMedioVidaTotal;
    [Header("Bandidos Pesado")]
    [SerializeField] float bandidoPesadoTiempoParaSpawnearMedio;
    [SerializeField] float bandidoPesadoMedioDañoCamioneta;
    [SerializeField] float bandidoPesadoMedioDañoJugador;
    [SerializeField] float bandidoPesadoMedioVidaTotal;
    [Header("Zombie")]
    [SerializeField] float zombieTiempoParaSpawnearMedio;
    [SerializeField] float zombieMedioDañoCamioneta;
    [SerializeField] float zombieMedioDañoJugador;
    [SerializeField] float zombieMedioVidaTotal;
    [Header("ZombieRapido")]
    [SerializeField] float zombieRapidoTiempoParaSpawnearMedio;
    [SerializeField] float zombieRapidoMedioDañoCamioneta;
    [SerializeField] float zombieRapidoMedioDañoJugador;
    [SerializeField] float zombieRapidoMedioVidaTotal;
    #endregion
    #region Parametros Nivel Dificil
    [Header("Nivel Dificil")]
    [Header("Bandidos")]
    [SerializeField] float bandidotiempoParaSpawnearDificil;
    [SerializeField] float bandidoDificilDañoCamioneta;
    [SerializeField] float bandidoDificilDañoJugador;
    [SerializeField] float bandidoDificilVidaTotal;
    [Header("Bandidos Pesado")]
    [SerializeField] float bandidoPesadoTiempoParaSpawnearDificil;
    [SerializeField] float bandidoPesadoDificilDañoCamioneta;
    [SerializeField] float bandidoPesadoDificilDañoJugador;
    [SerializeField] float bandidoPesadoDificilVidaTotal;
    [Header("Zombie")]
    [SerializeField] float zombieTiempoParaSpawnearDificil;
    [SerializeField] float zombieDificilDañoCamioneta;
    [SerializeField] float zombieDificilDañoJugador;
    [SerializeField] float zombieDificilVidaTotal;
    [Header("ZombieRapido")]
    [SerializeField] float zombieRapidoTiempoParaSpawnearDificil;
    [SerializeField] float zombieRapidoDificilDañoCamioneta;
    [SerializeField] float zombieRapidoDificilDañoJugador;
    [SerializeField] float zombieRapidoDificilVidaTotal;
    #endregion

    void Start()
    {
        Comenzar();
    }
    void Update()
    {
        if (gameOver)
        {
            ManagerStats.nivelDeDificutlad = 1;
            Time.timeScale = 0;
        }
    }
    public void Comenzar()
    {
        Time.timeScale = 1;
        gameOver = false;
        GameManagerJefe.jefeGameOver = false;
        GameManagerJefe2.jefe2GameOver = false;
        GameManager_Lvl3.gameOverLvl3= false;
        CheckDificultad();
        ResetObstaculos();
        LimpiarEnemigos();
        ResetJugador();
        ResetCamioneta();
        GestorDeAudio.instancia.PausarSonido("MacheteFuriaMusic");
        if (tieneCactus)
        {
            ResetCactus();
        }

        if (ManagerStats.instancia.primeraPartida == false)
        {
            ManagerStats.instancia.DevolverDatos();
        }
    }
    private void ResetJugador()
    {
        jugador.SetActive(true);
        jugador.GetComponent<Controlador_Vida>().vidaActual = jugador.GetComponent<Controlador_Vida>().vida;
        jugador.GetComponent<Player_Disparo>().municionActual = jugador.GetComponent<Player_Disparo>().municion;
        jugador.GetComponent<Controlador_Vida>().RellenarBarraDeVida();
        jugador.transform.position = jugadorSpawner.transform.position;
    }
    private void ResetCamioneta()
    {
        camioneta.SetActive(true);
        camioneta.GetComponent<Controlador_Vida>().vidaActual = camioneta.GetComponent<Controlador_Vida>().vida;
        camioneta.GetComponent<Controlador_Vida>().RellenarBarraDeVida();
        camioneta.transform.position = camionetaSpawner.transform.position;
    }
    private void ResetObstaculos()
    {
        if(tienePuente == false)
        {
            foreach (GameObject o in obstaculos)
            {
                Destroy(o);
            }
            obstaculos.Add(Instantiate(obstaculo, spawnObstaculos[UnityEngine.Random.Range(0, spawnObstaculos.Count)].position, Quaternion.Euler(0, 90, 0)));
        }
        else
        {
            foreach(GameObject madera in maderas)
            {
                Destroy(madera);
            }
            jugador.GetComponent<Player_DestruirObstaculos>().cantMadera = 0;
            GameObject.Find("Hud Manager").GetComponent<Controlador_HUD>().ActualizarContadorMadera();
            maderas.Add(Instantiate(obstaculo, new Vector3(-18.79f, 1.5f, 6.21f), Quaternion.identity));
            maderas.Add(Instantiate(obstaculo, new Vector3(-2.7f, 1.5f, -2.92f), Quaternion.identity));
            maderas.Add(Instantiate(obstaculo, new Vector3(17.44f, 1.5f, -8.63f), Quaternion.identity));
            resetPuente.Invoke();
            spawnersEnemigos[1].SetActive(false);
            spawnersEnemigos[2].SetActive(false);
            spawnersEnemigos[3].SetActive(false);
        }
    }
    private void ResetCactus()
    {
        foreach (GameObject cactus in listacactus)
        {
            Destroy(cactus);
        }
        listacactus.Add(Instantiate(cactus, new Vector3(-15.25f, 2.5f, 6.25f), Quaternion.identity));
        listacactus.Add(Instantiate(cactus, new Vector3(2f, 2.5f, -6f), Quaternion.identity));
        listacactus.Add(Instantiate(cactus, new Vector3(34f, 2.5f, -6f), Quaternion.identity));
        listacactus.Add(Instantiate(cactus, new Vector3(52.5f, 2.5f, 8.5f), Quaternion.identity));
    }
    private void LimpiarEnemigos()
    {
        foreach(GameObject lista in spawnersEnemigos)
        {
            Controlador_Spawner spawner;
            spawner = lista.GetComponent<Controlador_Spawner>();
            spawner.timer = spawner.cooldown;
            for (var i = lista.transform.childCount - 1; i >= 0; i--)
            {
                Destroy(lista.transform.GetChild(i).gameObject);
            }
        }
    }
    public void Restart(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (gameOver == false)
            {
                ManagerStats.instancia.DevolverDatosDefault();
            }
            Comenzar();
        }
    }
    private void CheckDificultad()
    {
        if(ManagerStats.nivelDeDificutlad == 1)
        {
            foreach (GameObject lista in spawnersEnemigos)
            {
                Controlador_Spawner spawner;
                spawner = lista.GetComponent<Controlador_Spawner>();
                if(spawner.objeto == bandido)
                {
                    spawner.cooldown = bandidoTiempoParaSpawnearFacil;
                    Bandidos_Comportamiento comportamiento = spawner.objeto.GetComponent<Bandidos_Comportamiento>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = bandidoFacilDañoCamioneta;
                    comportamiento.dañoJugador = bandidoFacilDañoJugador;
                    vida.vida = bandidoFacilVidaTotal;
                }
                else if(spawner.objeto == zombie)
                {
                    spawner.cooldown = zombieTiempoParaSpawnearFacil;
                    Zombie_Controlador comportamiento = spawner.objeto.GetComponent<Zombie_Controlador>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = zombieFacilDañoCamioneta;
                    comportamiento.dañoJugador = zombieFacilDañoJugador;
                    vida.vida = zombieFacilVidaTotal;
                }
                else if (spawner.objeto == zombieRapido)
                {
                    spawner.cooldown = zombieRapidoTiempoParaSpawnearFacil;
                    Zombie_Controlador comportamiento = spawner.objeto.GetComponent<Zombie_Controlador>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = zombieRapidoFacilDañoCamioneta;
                    comportamiento.dañoJugador = zombieRapidoFacilDañoJugador;
                    vida.vida = zombieRapidoFacilVidaTotal;
                }
                else if (spawner.objeto == bandidoPesado)
                {
                    spawner.cooldown = bandidoPesadoTiempoParaSpawnearFacil;
                    Bandidos_Comportamiento comportamiento = spawner.objeto.GetComponent<Bandidos_Comportamiento>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = bandidoPesadoFacilDañoCamioneta;
                    comportamiento.dañoJugador = bandidoPesadoFacilDañoJugador;
                    vida.vida = bandidoPesadoFacilVidaTotal;
                }
            }
        }
        else if (ManagerStats.nivelDeDificutlad == 2)
        {
            foreach (GameObject lista in spawnersEnemigos)
            {
                Controlador_Spawner spawner;
                spawner = lista.GetComponent<Controlador_Spawner>();
                if (spawner.objeto == bandido)
                {
                    spawner.cooldown = bandidoTiempoParaSpawnearMedio;
                    Bandidos_Comportamiento comportamiento = spawner.objeto.GetComponent<Bandidos_Comportamiento>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = bandidoMedioDañoCamioneta;
                    comportamiento.dañoJugador = bandidoMedioDañoJugador;
                    vida.vida = bandidoMedioVidaTotal;
                }
                else if (spawner.objeto == zombie)
                {
                    spawner.cooldown = zombieTiempoParaSpawnearMedio;
                    Zombie_Controlador comportamiento = spawner.objeto.GetComponent<Zombie_Controlador>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = zombieMedioDañoCamioneta;
                    comportamiento.dañoJugador = zombieMedioDañoJugador;
                    vida.vida = zombieMedioVidaTotal;
                }
                else if (spawner.objeto == zombieRapido)
                {
                    spawner.cooldown = zombieRapidoTiempoParaSpawnearMedio;
                    Zombie_Controlador comportamiento = spawner.objeto.GetComponent<Zombie_Controlador>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = zombieRapidoMedioDañoCamioneta;
                    comportamiento.dañoJugador = zombieRapidoMedioDañoJugador;
                    vida.vida = zombieRapidoMedioVidaTotal;
                }
                else if (spawner.objeto == bandidoPesado)
                {
                    spawner.cooldown = bandidoPesadoTiempoParaSpawnearMedio;
                    Bandidos_Comportamiento comportamiento = spawner.objeto.GetComponent<Bandidos_Comportamiento>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = bandidoPesadoMedioDañoCamioneta;
                    comportamiento.dañoJugador = bandidoPesadoMedioDañoJugador;
                    vida.vida = bandidoPesadoMedioVidaTotal;
                }
            }
        }
        else if (ManagerStats.nivelDeDificutlad >= 3)
        {
            foreach (GameObject lista in spawnersEnemigos)
            {
                Controlador_Spawner spawner;
                spawner = lista.GetComponent<Controlador_Spawner>();
                if (spawner.objeto == bandido)
                {
                    spawner.cooldown = bandidotiempoParaSpawnearDificil;
                    Bandidos_Comportamiento comportamiento = spawner.objeto.GetComponent<Bandidos_Comportamiento>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = bandidoDificilDañoCamioneta;
                    comportamiento.dañoJugador = bandidoDificilDañoJugador;
                    vida.vida = bandidoDificilVidaTotal;
                }
                else if (spawner.objeto == zombie)
                {
                    spawner.cooldown = zombieTiempoParaSpawnearDificil;
                    Zombie_Controlador comportamiento = spawner.objeto.GetComponent<Zombie_Controlador>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = zombieDificilDañoCamioneta;
                    comportamiento.dañoJugador = zombieDificilDañoJugador;
                    vida.vida = zombieDificilVidaTotal;
                }
                else if (spawner.objeto == zombieRapido)
                {
                    spawner.cooldown = zombieRapidoTiempoParaSpawnearDificil;
                    Zombie_Controlador comportamiento = spawner.objeto.GetComponent<Zombie_Controlador>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = zombieRapidoDificilDañoCamioneta;
                    comportamiento.dañoJugador = zombieRapidoDificilDañoJugador;
                    vida.vida = zombieRapidoDificilVidaTotal;
                }
                else if (spawner.objeto == bandidoPesado)
                {
                    spawner.cooldown = bandidoPesadoTiempoParaSpawnearDificil;
                    Bandidos_Comportamiento comportamiento = spawner.objeto.GetComponent<Bandidos_Comportamiento>();
                    Controlador_Vida vida = spawner.objeto.GetComponent<Controlador_Vida>();
                    comportamiento.dañoCamioneta = bandidoPesadoDificilDañoCamioneta;
                    comportamiento.dañoJugador = bandidoPesadoDificilDañoJugador;
                    vida.vida = bandidoPesadoDificilVidaTotal;
                }
            }
        }
    }
    public void AparecerSpawners()
    {
        spawnersEnemigos[1].SetActive(true);
        spawnersEnemigos[2].SetActive(true);
        spawnersEnemigos[3].SetActive(true);
    }
}
