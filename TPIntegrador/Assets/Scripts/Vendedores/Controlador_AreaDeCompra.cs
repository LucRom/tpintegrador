using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controlador_AreaDeCompra : MonoBehaviour
{
    [SerializeField] GameObject vendedor;
    [SerializeField] Shader outline;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            vendedor.GetComponent<MeshRenderer>().materials[1].SetFloat("_Scale", 1.1f);
            other.gameObject.GetComponent<Player_Dinero>().SetVendedor(vendedor);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            vendedor.GetComponent<MeshRenderer>().materials[1].SetFloat("_Scale", 0f);
            other.gameObject.GetComponent<Player_Dinero>().SetVendedor(null);
        }
    }
}
