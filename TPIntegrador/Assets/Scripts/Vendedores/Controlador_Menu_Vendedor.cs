using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Controlador_Menu_Vendedor : MonoBehaviour
{
    [Header("GameObjects")]
    public GameObject jugador;
    private Player_Dinero dinero;
    [Header("Botones")]
    [SerializeField] public Button btnVida;
    [SerializeField] Button btnVelocidad;
    [SerializeField] Button btnFuerza;
    [Header("Textos")]
    [SerializeField] TMPro.TMP_Text txtprecioVida;
    [SerializeField] TMPro.TMP_Text txtprecioVelocidad;
    [SerializeField] TMPro.TMP_Text txtprecioFuerza;
    [Header("Precios")]
    [SerializeField] public int precioVida;
    [SerializeField] public int precioVelocidad;
    [SerializeField] public int precioFuerza;
    [Header("Niveles")]
    public int lvlVida = 1;
    public int lvlVelocidad = 1;
    public int lvlFuerza = 1;
    public bool lvlMaxVida = false;
    public bool lvlMaxVelocidad = false;
    public bool lvlMaxFuerza = false;
    [Header("Extras")]
    float nuevacadencia = 0.3f;

    private void Start()
    {
        txtprecioVida.text = "$" + precioVida.ToString();
        txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
        txtprecioFuerza.text = "$" + precioFuerza.ToString();
        ManagerStats.instancia.DevolverDatosTiendaVendedor();
        CheckPrecios();
    }
    private void Update()
    {
        if(jugador != null)
        {
            dinero = jugador.GetComponent<Player_Dinero>();
        }
    }
    public void ComprarVida()
    {
        if (lvlMaxVida == false && dinero.dinero >= precioVelocidad)
        {
            GestorDeAudio.instancia.ReproducirSonido("SonidoCompra");
            jugador.GetComponent<Controlador_Vida>().vida += 25;
            jugador.GetComponent<Player_Dinero>().dinero -= precioVida;
            lvlVida++;
            switch (lvlVida)
            {
                case 2:
                    precioVida += 100;
                    txtprecioVida.text = "$" + precioVida.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaVendedor();
                    break;
                case 3:
                    precioVida += 200;
                    txtprecioVida.text = "$" + precioVida.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaVendedor();
                    break;
                case 4:
                    txtprecioVida.text = "Max";
                    btnVida.GetComponent<CanvasGroup>().alpha= 0.2f;
                    lvlMaxVida = true;
                    ManagerStats.instancia.GuardarDatosTiendaVendedor();
                    break;
            }
        }
    }
    public void ComprarFuerza()
    {
        if (lvlMaxFuerza == false && dinero.dinero >= precioFuerza)
        {
            GestorDeAudio.instancia.ReproducirSonido("SonidoCompra");
            jugador.GetComponent<Player_Disparo>().cadencia = nuevacadencia;
            jugador.GetComponent<Player_Disparo>().municion += 10;
            jugador.GetComponent<Player_Dinero>().dinero -= precioFuerza;
            lvlFuerza++;
            switch (lvlFuerza)
            {
                case 2:
                    precioFuerza += 100;
                    nuevacadencia -= 0.1f;
                    txtprecioFuerza.text = "$" + precioFuerza.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaVendedor();
                    break;
                case 3:
                    precioFuerza += 200;
                    nuevacadencia -= 0.1f;
                    txtprecioFuerza.text = "$" + precioFuerza.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaVendedor();
                    break;
                case 4:
                    txtprecioFuerza.text = "Max";
                    btnFuerza.GetComponent<CanvasGroup>().alpha = 0.2f;
                    lvlMaxFuerza = true;
                    ManagerStats.instancia.GuardarDatosTiendaVendedor();
                    break;
            }
        }
    }
    public void ComprarVelocidad()
    {
        if (lvlMaxVelocidad == false && dinero.dinero >= precioVelocidad)
        {
            GestorDeAudio.instancia.ReproducirSonido("SonidoCompra");
            jugador.GetComponent<Player_Movimiento>().velocidad += 2;
            jugador.GetComponent<Player_DestruirObstaculos>().dañoAObstaculos += 2;
            jugador.GetComponent<Player_Dinero>().dinero -= precioVelocidad;
            lvlVelocidad++;
            switch (lvlVelocidad)
            {
                case 2:
                    precioVelocidad += 100;
                    txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaVendedor();
                    break;
                case 3:
                    precioVelocidad += 200;
                    txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaVendedor();
                    break;
                case 4:
                    txtprecioVelocidad.text = "Max";
                    btnVelocidad.GetComponent<CanvasGroup>().alpha = 0.2f;
                    lvlMaxVelocidad = true;
                    ManagerStats.instancia.GuardarDatosTiendaVendedor();
                    break;
            }
        }
    }
    private void CheckPrecios()
    {
        switch (lvlVida)
        {
            case 2:
                txtprecioVida.text = "$" + precioVida.ToString();
                break;
            case 3:
                txtprecioVida.text = "$" + precioVida.ToString();
                break;
            case 4:
                txtprecioVida.text = "Max";
                btnVida.GetComponent<CanvasGroup>().alpha = 0.2f;
                lvlMaxVida = true;
                ManagerStats.instancia.GuardarDatosTiendaVendedor();
                break;
        }
        switch (lvlFuerza)
        {
            case 2:
                txtprecioFuerza.text = "$" + precioFuerza.ToString();
                break;
            case 3:
                txtprecioFuerza.text = "$" + precioFuerza.ToString();
                break;
            case 4:
                txtprecioFuerza.text = "Max";
                btnFuerza.GetComponent<CanvasGroup>().alpha = 0.2f;
                lvlMaxFuerza = true;
                ManagerStats.instancia.GuardarDatosTiendaVendedor();
                break;
        }
        switch (lvlVelocidad)
        {
            case 2:
                txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
                break;
            case 3:
                txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
                break;
            case 4:
                txtprecioVelocidad.text = "Max";
                btnVelocidad.GetComponent<CanvasGroup>().alpha = 0.2f;
                lvlMaxVelocidad = true;
                ManagerStats.instancia.GuardarDatosTiendaVendedor();
                break;
        }
    }
}
