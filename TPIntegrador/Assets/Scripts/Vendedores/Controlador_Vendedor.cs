using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class Controlador_Vendedor : MonoBehaviour
{
    public bool vendedor;
    public bool estaComprando;
    [SerializeField] GameObject menuDeCompras;

    void Start()
    {
        estaComprando = false;
        menuDeCompras.SetActive(false);
    }
    void Update()
    {
        if(estaComprando)
        {
            menuDeCompras.SetActive(true);
        }
        if(estaComprando == false)
        {
            estaComprando = false;
            menuDeCompras.SetActive(false);
        }
    }
    public void SetJugador(GameObject pjugador)
    {
        if(vendedor)
        {
            menuDeCompras.GetComponent<Controlador_Menu_Vendedor>().jugador = pjugador;
            //EventSystem.current.SetSelectedGameObject(menuDeCompras.GetComponent<Controlador_Menu_Vendedor>().btnVida.gameObject);
            //EventSystem.current.firstSelectedGameObject = menuDeCompras.GetComponent<Controlador_Menu_Vendedor>().btnVida.gameObject;
        }
        else
        {
            menuDeCompras.GetComponent<Controlador_Menu_Mecanico>().jugador = pjugador;
            //EventSystem.current.SetSelectedGameObject(menuDeCompras.GetComponent<Controlador_Menu_Mecanico>().btnResistencia.gameObject);
            //EventSystem.current.firstSelectedGameObject = menuDeCompras.GetComponent<Controlador_Menu_Mecanico>().btnResistencia.gameObject;
        }

    }

}
