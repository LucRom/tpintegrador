using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class Controlador_Menu_Mecanico : MonoBehaviour
{
    [Header("GameObjects")]
    public GameObject jugador;
    public GameObject camioneta;
    private Player_Dinero dinero;
    [Header("Botones")]
    [SerializeField] public Button btnResistencia;
    [SerializeField] Button btnVelocidad;
    [SerializeField] Button btnSegundaOportunidad;
    [Header("Textos")]
    [SerializeField] TMPro.TMP_Text txtprecioResistencia;
    [SerializeField] TMPro.TMP_Text txtprecioVelocidad;
    [SerializeField] TMPro.TMP_Text txtprecioSegundaOportunidad;
    [Header("Precios")]
    [SerializeField] public int precioResistencia;
    [SerializeField] public int precioVelocidad;
    [SerializeField] public int precioSegundaOportunidad;
    [Header("Niveles")]
    public int lvlResistencia = 1;
    public int lvlVelocidad = 1;
    public bool lvlMaxResistencia = false;
    public bool lvlMaxVelocidad = false;
    private bool lvlMaxSegundaOportunidad = false;

    private void Start()
    {
        txtprecioResistencia.text = "$" + precioResistencia.ToString();
        txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
        txtprecioSegundaOportunidad.text = "$" + precioSegundaOportunidad.ToString();
        ManagerStats.instancia.DevolverDatosTiendaMecanico();
        CheckPrecios();
    }
    private void Update()
    {
        if (jugador != null)
        {
            dinero = jugador.GetComponent<Player_Dinero>();
        }
    }
    public void ComprarResistencia()
    {
        if (lvlMaxResistencia == false && dinero.dinero >= precioResistencia)
        {
            GestorDeAudio.instancia.ReproducirSonido("SonidoCompra");
            camioneta.GetComponent<Controlador_Vida>().vida += 25;
            jugador.GetComponent<Player_Dinero>().dinero -= precioResistencia;
            lvlResistencia++;
            switch (lvlResistencia)
            {
                case 2:
                    precioResistencia += 100;
                    txtprecioResistencia.text = "$" + precioResistencia.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaMecanico();
                    break;
                case 3:
                    precioResistencia += 200;
                    txtprecioResistencia.text = "$" + precioResistencia.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaMecanico();
                    break;
                case 4:
                    txtprecioResistencia.text = "Max";
                    btnResistencia.GetComponent<CanvasGroup>().alpha = 0.2f;
                    lvlMaxResistencia = true;
                    ManagerStats.instancia.GuardarDatosTiendaMecanico();
                    break;
            }
            ManagerStats.instancia.GuardarDatosTiendaMecanico();
        }
    }
    public void ComprarSegundaOportunidad()
    {
        if (lvlMaxSegundaOportunidad == false && dinero.dinero >= precioSegundaOportunidad)
        {
            GestorDeAudio.instancia.ReproducirSonido("SonidoCompra");
            camioneta.GetComponent<Camioneta_Movimiento>().segundaOportunidad = true;
            jugador.GetComponent<Player_Dinero>().dinero -= precioSegundaOportunidad;
            lvlMaxSegundaOportunidad = true;
            btnSegundaOportunidad.GetComponent<CanvasGroup>().alpha = 0.2f;
        }
    }
    public void ComprarVelocidad()
    {
        if (lvlMaxVelocidad == false && dinero.dinero >= precioVelocidad)
        {
            GestorDeAudio.instancia.ReproducirSonido("SonidoCompra");
            camioneta.GetComponent<Camioneta_Movimiento>().velocidad += 1;
            jugador.GetComponent<Player_Dinero>().dinero -= precioVelocidad;
            lvlVelocidad++;
            switch (lvlVelocidad)
            {
                case 2:
                    precioVelocidad += 100;
                    txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaMecanico();
                    break;
                case 3:
                    precioVelocidad += 200;
                    txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
                    ManagerStats.instancia.GuardarDatosTiendaMecanico();
                    break;
                case 4:
                    txtprecioVelocidad.text = "Max";
                    btnVelocidad.GetComponent<CanvasGroup>().alpha = 0.2f;
                    lvlMaxVelocidad = true;
                    ManagerStats.instancia.GuardarDatosTiendaMecanico();
                    break;
            }
        }
    }
    private void CheckPrecios()
    {
        switch (lvlResistencia)
        {
            case 2:
                txtprecioResistencia.text = "$" + precioResistencia.ToString();
                break;
            case 3:
                txtprecioResistencia.text = "$" + precioResistencia.ToString();
                break;
            case 4:
                txtprecioResistencia.text = "Max";
                btnResistencia.GetComponent<CanvasGroup>().alpha = 0.2f;
                lvlMaxResistencia = true;
                break;
        }
        switch (lvlVelocidad)
        {
            case 2:
                txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
                break;
            case 3:
                txtprecioVelocidad.text = "$" + precioVelocidad.ToString();
                break;
            case 4:
                txtprecioVelocidad.text = "Max";
                btnVelocidad.GetComponent<CanvasGroup>().alpha = 0.2f;
                lvlMaxVelocidad = true;
                break;
        }
    }
}

