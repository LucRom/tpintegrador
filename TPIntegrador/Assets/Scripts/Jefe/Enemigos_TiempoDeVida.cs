using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigos_TiempoDeVida : MonoBehaviour
{
    public float tiempoDeVidaEnemigos = 5;
    private float timer;

    private void Start()
    {
        timer = tiempoDeVidaEnemigos;
        Debug.Log(tiempoDeVidaEnemigos);
    }
    private void Update()
    {
        timer-= Time.deltaTime;
        if(timer <= 0)
        {
            Destroy(gameObject);
        }
    }
}
