using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Controlador_Jefe : MonoBehaviour
{
    private float timerMostrar;
    private float timerOcultar;
    [SerializeField] GameObject escudo;
    [SerializeField] float tiempoEscudoMostrar;
    [SerializeField] float tiempoEscudoOcultar;
    private bool ocultarEscudo;
    private void Start()
    {
        ocultarEscudo = false;
        timerMostrar = tiempoEscudoMostrar;
        timerOcultar = tiempoEscudoOcultar;
    }
    private void Update()
    {
        if(ocultarEscudo == false)
        {
            timerOcultar -= Time.deltaTime;
            if(timerOcultar <= 0)
            {
                escudo.gameObject.SetActive(false);
                ocultarEscudo = true;
                timerOcultar = tiempoEscudoOcultar;
            }
        }
        else
        {
            timerMostrar -= Time.deltaTime;
            if(timerMostrar <=0)
            {
                escudo.gameObject.SetActive(true);
                ocultarEscudo = false;
                timerMostrar = tiempoEscudoMostrar;
            }
        }
    }

}
