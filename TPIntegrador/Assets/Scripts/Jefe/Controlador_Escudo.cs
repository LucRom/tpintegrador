using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Escudo : MonoBehaviour
{
    [SerializeField] GameObject jefe;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoJugador(jefe.GetComponent<Bandidos_Comportamiento>().daņoJugador);
        }
    }
}
