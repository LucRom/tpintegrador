using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.VFX;

public class Jefe_Vida : MonoBehaviour
{
    [Header("Vida")]
    [SerializeField] public float vida;
    [SerializeField] bool tieneBarraDeVida;
    [SerializeField] bool barraDeVidaEstatica;
    [SerializeField] Image barraDeVida;
    [SerializeField] Image barraDeVidaFondo;
    [HideInInspector] public float vidaActual;
    [Header("Efectos")]
    [SerializeField] bool tieneSangre;
    [SerializeField] VisualEffect sangre;
    [Header("Jefe")]
    [SerializeField] bool jefe2;
    void Start()
    {
        if (tieneSangre)
        {
            sangre.Stop();
        }
        vida = ManagerStats.vidaJefe;
        vidaActual = vida;
    }
    void Update()
    {
        if (tieneBarraDeVida && barraDeVidaEstatica == false)
        {
            barraDeVidaFondo.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
            barraDeVida.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
        }
    }
    public void RecibirDaņoDesaparecer(float daņo)
    {
        vidaActual -= daņo;
        if (tieneBarraDeVida)
        {
            barraDeVida.fillAmount = vidaActual / vida;
        }
        if (vidaActual <= 0)
        {
            GetComponent<Enemigo_Dinero>().DarDinero();
            GestorDeAudio.instancia.PausarSonido("MacheteFuriaMusic");
            ManagerStats.instancia.GuardarDatos();
            ManagerStats.nivelEspecial = false;
            ManagerStats.instancia.AumentarVidaJefe();
            if (jefe2)
            {
                ManagerStats.instancia.SumarDificultad2Jefe();
            }
            SceneManager.LoadScene("Tienda");   
            Destroy(this.gameObject);
        }
    }
    public void MostrarSangre(Transform posicion)
    {
        sangre.Play();
        sangre.gameObject.transform.position = posicion.position;
    }
}
