using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManagerJefe : MonoBehaviour
{
    [SerializeField] GameObject jugador;
    [SerializeField] List<GameObject> spawnersEnemigos = new List<GameObject>();
    [SerializeField] List<string> listaNombres = new List<string>();
    [SerializeField] TMPro.TMP_Text txtJefeNombre;
    [SerializeField] float tiempoDeVidaEnemigos;
    private float timer;
    public static bool jefeGameOver;
    void Start()
    {
        Comienzo();
    }
    void Update()
    {
        if(jefeGameOver)
        {
            Time.timeScale = 0;
        }
        if(jugador.activeInHierarchy == false)
        {
            jefeGameOver = true;
        }

        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            foreach (GameObject lista in spawnersEnemigos)
            {
                Controlador_Spawner spawner;
                spawner = lista.GetComponent<Controlador_Spawner>();
                spawner.timer = spawner.cooldown;
                for (var i = lista.transform.childCount - 1; i >= 0; i--)
                {
                    Destroy(lista.transform.GetChild(i).gameObject);
                }
            }
            timer = tiempoDeVidaEnemigos;
        }
    }
    private void Comienzo()
    {
        timer = tiempoDeVidaEnemigos;
        ManagerStats.instancia.DevolverDatos();
        jefeGameOver = false;
        Time.timeScale = 1;
        txtJefeNombre.text = listaNombres[UnityEngine.Random.Range(0, listaNombres.Count)];
        foreach (GameObject lista in spawnersEnemigos)
        {
            Controlador_Spawner spawner;
            spawner = lista.GetComponent<Controlador_Spawner>();
            spawner.timer = spawner.cooldown;
            for (var i = lista.transform.childCount - 1; i >= 0; i--)
            {
                Destroy(lista.transform.GetChild(i).gameObject);
            }
        }
        foreach (GameObject lista in spawnersEnemigos)
        {
            GameObject enemigo = lista.GetComponent<Controlador_Spawner>().objeto;
            enemigo.GetComponent<Enemigo_Controllador_Movimiento>().soloBuscarAlJugador = true;
        }
    }
    public void Restart(InputAction.CallbackContext context)
    {
        if (context.performed && jefeGameOver)
        {
            SceneManager.LoadScene("Lvl1");
        }
    }
}
