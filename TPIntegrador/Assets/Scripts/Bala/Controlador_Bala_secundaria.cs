using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.HID;

public class Controlador_Bala_secundaria : MonoBehaviour
{
    [SerializeField] GameObject Bala;
    public float daño;
    public float tiempoParaExplotar;
    private float timerExplotar;
    private float tiempoParaDesaparecer = 0.3f;
    private float timerDesaparecer;
    private bool puedeHacerDaño;
    private void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("GranadaAExplotar");
        puedeHacerDaño = false;
        timerExplotar = tiempoParaExplotar;
        timerDesaparecer = tiempoParaDesaparecer;
        gameObject.GetComponent<SphereCollider>().enabled = false;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
    }
    private void Update()
    {
        timerExplotar -= Time.deltaTime;
        if(timerExplotar <= 0)
        {
            gameObject.GetComponent<SphereCollider>().enabled = true;
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            puedeHacerDaño = true;
        }

        if(puedeHacerDaño)
        {
            timerDesaparecer-= Time.deltaTime;
            if(timerDesaparecer<= 0)
            {
                GestorDeAudio.instancia.ReproducirSonido("explosion");
                Destroy(Bala);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemigo"))
        {
            other.gameObject.GetComponent<Controlador_Vida>().RecibirDañoDesaparecer(daño);
            other.gameObject.GetComponent<Enemigo_Dinero>().DarDinero();
        }
    }
}
