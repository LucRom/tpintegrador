using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Bala_Torreta : MonoBehaviour
{
    private Rigidbody rb;
    public float velocidad;
    public float daņo;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * velocidad;
    }
    void Update()
    {
        Destroy(this.gameObject, 5);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoDesaparecer(daņo);
            if (collision.gameObject.GetComponent<Controlador_Vida>().vidaActual <= 0)
            {
                collision.gameObject.GetComponent<Enemigo_Dinero>().DarDinero();
                GestorDeAudio.instancia.ReproducirSonido("Killsound");
            }
            else
            {
                GestorDeAudio.instancia.ReproducirSonido("Hit Marker");
            }
        }
        Destroy(this.gameObject);
    }
}
