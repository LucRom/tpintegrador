using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Bala_Explosiva_Enemiga : MonoBehaviour
{
    [SerializeField] GameObject Bala;
    public float daño;
    public float tiempoParaExplotar;
    private float timerExplotar;
    private float tiempoParaDesaparecer = 0.3f;
    private float timerDesaparecer;
    private bool puedeHacerDaño;
    Rigidbody rb;
    private void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("GranadaAExplotar");
        rb = Bala.GetComponent<Rigidbody>();
        rb.velocity = transform.forward * 10;
        puedeHacerDaño = false;
        timerExplotar = tiempoParaExplotar;
        timerDesaparecer = tiempoParaDesaparecer;
        gameObject.GetComponent<SphereCollider>().enabled = false;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
    }
    private void Update()
    {
        timerExplotar -= Time.deltaTime;
        if (timerExplotar <= 0)
        {
            gameObject.GetComponent<SphereCollider>().enabled = true;
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            puedeHacerDaño = true;
        }

        if (puedeHacerDaño)
        {
            timerDesaparecer -= Time.deltaTime;
            if (timerDesaparecer <= 0)
            {
                GestorDeAudio.instancia.ReproducirSonido("explosion");
                Destroy(Bala);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Controlador_Vida>().RecibirDañoJugador(daño);
        }
    }
}
