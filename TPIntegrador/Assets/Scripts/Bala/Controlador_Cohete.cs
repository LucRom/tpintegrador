using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Cohete : MonoBehaviour
{
    private Rigidbody rb;
    public float velocidad;
    public float daño;
    public bool disparoJugador;
    private Player_HUD playerHud;
    void Start()
    {
        if (disparoJugador)
        {
            GestorDeAudio.instancia.ReproducirSonido("LanzaCoheteDisparo");
            playerHud = GameObject.Find("Jugador").GetComponent<Player_HUD>();
        }
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * velocidad;
        GameObject.Find("Game Manager").GetComponent<GameManagerJefe2>().PintarTorresNaranja();
    }
    private void Update()
    {
        Destroy(this.gameObject, 5);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstaculo") && disparoJugador)
        {
            GestorDeAudio.instancia.ReproducirSonido("explosion cohete");
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDañoOcultar(daño);
            if (collision.gameObject.GetComponent<Controlador_Vida>().vidaActual <= 0)
            {
                GestorDeAudio.instancia.ReproducirSonido("Killsound");
                playerHud.MostrarHitMarkerKill();
            }
            else
            {
                GestorDeAudio.instancia.ReproducirSonido("Hit Marker");
                playerHud.MostrarHitMarker();
            }
        }
        if (collision.gameObject.CompareTag("Jefe") && disparoJugador)
        {
            collision.gameObject.GetComponent<Jefe_Vida>().RecibirDañoDesaparecer(daño);
            collision.gameObject.GetComponent<Jefe_Vida>().MostrarSangre(collision.transform);
            if (collision.gameObject.GetComponent<Jefe_Vida>().vidaActual <= 0)
            {
                collision.gameObject.GetComponent<Enemigo_Dinero>().DarDinero();
                GestorDeAudio.instancia.ReproducirSonido("Killsound");
                playerHud.MostrarHitMarkerKill();
            }
            else
            {
                GestorDeAudio.instancia.ReproducirSonido("Hit Marker");
                playerHud.MostrarHitMarker();
            }
        }
        Destroy(this.gameObject);
    }
}
