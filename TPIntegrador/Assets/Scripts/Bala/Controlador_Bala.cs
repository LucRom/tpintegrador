using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;
using UnityEngine.Events;

public class Controlador_Bala : MonoBehaviour
{
    private Rigidbody rb;
    public float velocidad;
    public float daņo;
    public bool disparoJugador;
    private Player_HUD playerHud;
    void Start()
    {
        if (disparoJugador)
        {
            GestorDeAudio.instancia.ReproducirSonido("Disparo");
            playerHud = GameObject.Find("Jugador").GetComponent<Player_HUD>();
        }
        rb= GetComponent<Rigidbody>();
        rb.velocity = transform.forward * velocidad;
    }
    private void Update()
    {
        Destroy(this.gameObject,5);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo") && disparoJugador)
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoDesaparecer(daņo);
            collision.gameObject.GetComponent<Controlador_Vida>().MostrarSangre(collision.transform);
            if(collision.gameObject.GetComponent<Controlador_Vida>().vidaActual <= 0)
            {
                collision.gameObject.GetComponent<Enemigo_Dinero>().DarDinero();
                GestorDeAudio.instancia.ReproducirSonido("Killsound");
                playerHud.MostrarHitMarkerKill();
            }
            else
            {
                GestorDeAudio.instancia.ReproducirSonido("Hit Marker");
                playerHud.MostrarHitMarker();
            }
        }
        if (collision.gameObject.CompareTag("Jefe") && disparoJugador)
        {
            collision.gameObject.GetComponent<Jefe_Vida>().RecibirDaņoDesaparecer(daņo);
            collision.gameObject.GetComponent<Jefe_Vida>().MostrarSangre(collision.transform);
            if (collision.gameObject.GetComponent<Jefe_Vida>().vidaActual <= 0)
            {
                collision.gameObject.GetComponent<Enemigo_Dinero>().DarDinero();
                GestorDeAudio.instancia.ReproducirSonido("Killsound");
                playerHud.MostrarHitMarkerKill();
            }
            else
            {
                GestorDeAudio.instancia.ReproducirSonido("Hit Marker");
                playerHud.MostrarHitMarker();
            }
        }
        Destroy(this.gameObject);
    }
}
