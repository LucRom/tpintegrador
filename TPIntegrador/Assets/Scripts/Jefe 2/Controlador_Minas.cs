using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Minas : MonoBehaviour
{
    public float da�o;
    private float timerParalizacion;
    [SerializeField] private float tiempoParalizacion;
    private bool paralizar;
    private GameObject jugador;
    private float velocidadAux;
    private void Start()
    {
        timerParalizacion = tiempoParalizacion;
        paralizar = false;
    }
    private void Update()
    {
        if (paralizar)
        {
            jugador.GetComponent<Player_Movimiento>().velocidadMod = 0;
            timerParalizacion -= Time.deltaTime;
            if (timerParalizacion <= 0)
            {
                jugador.GetComponent<Player_Movimiento>().velocidadMod = velocidadAux;
                Destroy(gameObject);
            }
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GestorDeAudio.instancia.ReproducirSonido("MinaSfx");
            jugador = other.gameObject;
            jugador.GetComponent<Controlador_Vida>().RecibirDa�oJugador(da�o);
            velocidadAux = jugador.GetComponent<Player_Movimiento>().velocidadMod;
            paralizar = true;
        }
    }
}
