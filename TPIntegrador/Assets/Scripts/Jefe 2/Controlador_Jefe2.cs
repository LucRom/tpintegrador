using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Jefe2 : MonoBehaviour
{
    [Header("GameObjects")]
    [SerializeField] GameObject jugador;
    [SerializeField] GameObject arma;
    [SerializeField] GameObject laser;
    [SerializeField] GameObject mina;
    [Header("Fase 1")]
    [Header("Laser")]
    [SerializeField] Material laserMaterial;
    [SerializeField] Material laserDisparo;
    [Header("Disparo")]
    [SerializeField] float cooldownDisparo;
    [SerializeField] float daņo;
    private float timer;
    private bool vaADisparar;
    private LineRenderer lR;
    [Header("Lugares")]
    [SerializeField] List<Transform> lugares = new List<Transform>();
    private int lugarActual =0;
    private bool torresDisponibles;
    [Header("Fase 2")]
    [Header("Dash")]
    [SerializeField] float potenciaDash;
    [SerializeField] float cooldownDash;
    [SerializeField] float daņoFase2;
    private float timerDash;
    private Rigidbody rb;
    [Header("Minas")]
    [SerializeField] float cooldownMinas;
    private float timerMinas;

    void Start()
    {
        if (ManagerStats.nivel2Jefe == 1)
        {
            cooldownDisparo = 5;
        }
        else if (ManagerStats.nivel2Jefe == 2)
        {
            cooldownDisparo = 3;
        }
        else if (ManagerStats.nivel2Jefe >= 3)
        {
            cooldownDisparo = 2;
        }
        timer = cooldownDisparo;
        timerDash = cooldownDash;
        timerMinas = cooldownMinas; 
        vaADisparar = false;
        lR = laser.GetComponent<LineRenderer>();
        torresDisponibles = true;
        GameManagerJefe2.fase1 = true;
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        if (GameManagerJefe2.fase1)
        {
            Estado1();
        }
        else
        {
            Estado2();
        }
    }
    private void Estado1()
    {
        rb.constraints = RigidbodyConstraints.FreezePosition;
        CheckTorre();
        if (vaADisparar == false)
        {
            transform.LookAt(jugador.transform.position);
            lR.material = laserMaterial;
            lR.SetPosition(0, transform.position);
            lR.SetPosition(1, jugador.transform.position);
        }
        timer -= Time.deltaTime;
        if (timer <= (cooldownDisparo / 4))
        {
            GestorDeAudio.instancia.ReproducirSonido("AlertaDisparoSfx");
            vaADisparar = true;
            lR.material = laserDisparo;
        }
        if (timer <= 0)
        {
            Disparar();
            timer = cooldownDisparo;
            vaADisparar = false;
        }
    }
    private void Disparar()
    {
        GestorDeAudio.instancia.ReproducirSonido("SniperSfx");
        RaycastHit hit;

        if (Physics.Raycast(arma.transform.position, arma.transform.forward, out hit))
        {
            if (hit.transform.gameObject.CompareTag("Player"))
            {
                hit.transform.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoJugador(daņo);
            }
        }
    }
    private void CheckTorre()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, Vector3.down, out hit, 6) && torresDisponibles)
        {
            if (!hit.transform.CompareTag("Obstaculo"))
            {
                lugarActual++;
                transform.position = lugares[lugarActual].transform.position;
            }
        }
        if(lugarActual == 4)
        {
            torresDisponibles = false;
            transform.position = lugares[4].transform.position;
            GameManagerJefe2.fase1 = false;
        }
    }
    private void Estado2()
    {
        rb.constraints = RigidbodyConstraints.None;
        rb.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationX;
        laser.gameObject.SetActive(false);
        arma.gameObject.SetActive(false);
        timerDash -= Time.deltaTime;
        transform.LookAt(jugador.transform.position);
        if (timerDash <= 0)
        {
            rb.AddForce(transform.forward * potenciaDash, ForceMode.Impulse);
            timerDash = cooldownDash;
        }
        timerMinas -= Time.deltaTime;
        if(timerMinas <= 0)
        {
            Instantiate(mina, new Vector3(transform.position.x, 0.5f, transform.position.z), Quaternion.identity);
            timerMinas = cooldownMinas;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoJugador(daņoFase2);
        }
    }
}
