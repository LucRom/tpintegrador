using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManagerJefe2 : MonoBehaviour
{
    [Header("Estado")]
    [SerializeField] public static bool fase1;
    [Header("GameObjects")]
    [SerializeField] GameObject jugador;
    [SerializeField] GameObject bandido;
    [SerializeField] GameObject zombie;
    [SerializeField] GameObject zombieRapido;
    [SerializeField] GameObject bandidoPesado;
    [SerializeField] GameObject itemLanzaCohete;
    [Header("Lista")]
    [SerializeField] List<GameObject> torres = new List<GameObject>();
    [SerializeField] List<GameObject> spawnersEnemigos = new List<GameObject>();
    [SerializeField] List<GameObject> listaEnemigos = new List<GameObject>();
    [SerializeField] List<string> listaNombres = new List<string>();
    [Header("Textos")]
    [SerializeField] TMPro.TMP_Text txtJefeNombre;
    [Header("Fase 1")]
    [Header("Rondas")]
    [SerializeField] float cooldownSiguienteRonda;
    private float timer;
    public static int enemigosEliminados =0;
    private bool estaEnRonda;
    private bool siguienteRonda;
    public static bool jefe2GameOver;
    [SerializeField] Material matNaranja;
    [SerializeField] Material matRojo;
    [Header("Fase 2")]
    [SerializeField] GameObject zonaReducida;
    [SerializeField] float cooldownZona;
    [SerializeField] float cooldownTiempoZona;
    private float timerZona;
    void Start()
    {
        Comienzo();
    }

    void Update()
    {
        if (jefe2GameOver)
        {
            Time.timeScale = 0;
        }
        if (jugador.activeInHierarchy == false)
        {
            jefe2GameOver = true;
        }
        if(fase1 == true)
        {
            Rondas();
        }
        else
        {
            Fase2();
        }
    }
    private void Rondas()
    {
        if (siguienteRonda)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                estaEnRonda = false;
                siguienteRonda= false;
            }
        }

        if(estaEnRonda == false)
        {
            listaEnemigos.Clear();
            listaEnemigos.Add(Instantiate(bandidoPesado, spawnersEnemigos[0].transform));
            listaEnemigos.Add(Instantiate(zombie, spawnersEnemigos[1].transform));
            listaEnemigos.Add(Instantiate(bandido, spawnersEnemigos[2].transform));
            listaEnemigos.Add(Instantiate(zombie, spawnersEnemigos[3].transform));
            listaEnemigos.Add(Instantiate(zombieRapido, spawnersEnemigos[4].transform));
            listaEnemigos.Add(Instantiate(zombie, spawnersEnemigos[5].transform));
            listaEnemigos.Add(Instantiate(bandido, spawnersEnemigos[6].transform));
            listaEnemigos.Add(Instantiate(bandido, spawnersEnemigos[7].transform));
            foreach (GameObject lista in listaEnemigos)
            {
                lista.GetComponent<Controlador_Vida>().jefe2 = true;
                lista.GetComponent<Enemigo_Controllador_Movimiento>().soloBuscarAlJugador = true;
            }
            estaEnRonda = true;
        }
        if(enemigosEliminados >= spawnersEnemigos.Count)
        {
            if (fase1)
            {
                Instantiate(itemLanzaCohete, new Vector3(0, 1.5f, 0), Quaternion.identity);
            }
            enemigosEliminados = 0;
            siguienteRonda = true;
            timer = cooldownSiguienteRonda;
        }
    }
    private void Fase2()
    {
        Rondas();
        timerZona -= Time.deltaTime;
        if(timerZona <= 0)
        {
            GameObject zonaActual;
            zonaActual = Instantiate(zonaReducida, jugador.transform.position, Quaternion.identity);
            if(ManagerStats.nivel2Jefe == 1)
            {
                zonaActual.transform.localScale = new Vector3(8, 1, 8);
            }
            else if (ManagerStats.nivel2Jefe == 2)
            {
                zonaActual.transform.localScale = new Vector3(6, 1, 6);
            }
            else if (ManagerStats.nivel2Jefe >= 3)
            {
                zonaActual.transform.localScale = new Vector3(4, 1, 4);
            }
            timerZona = cooldownZona;
            Destroy(zonaActual, 5);
        }
    }
    private void Comienzo()
    {
        fase1 = true;
        estaEnRonda = false;
        siguienteRonda= false;
        timer = cooldownSiguienteRonda;
        timerZona = cooldownZona;
        ManagerStats.instancia.DevolverDatos();
        jefe2GameOver = false;
        Time.timeScale = 1;
        txtJefeNombre.text =listaNombres[UnityEngine.Random.Range(0, listaNombres.Count)];
        foreach (GameObject lista in listaEnemigos)
        {
            for (var i = lista.transform.childCount - 1; i >= 0; i--)
            {
                Destroy(lista.transform.GetChild(i).gameObject);
            }
        }
        foreach (GameObject lista in torres)
        {
            lista.gameObject.SetActive(true);
        }
    }
    public void Restart(InputAction.CallbackContext context)
    {
        if (context.performed && jefe2GameOver)
        {
            SceneManager.LoadScene("Lvl1");
        }
    }
    public void Pasar2Fase(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            foreach (GameObject torre in torres)
            {
                torre.SetActive(false);
            }
        }
    }
    public void PintarTorresRojo()
    {
        foreach (GameObject torre in torres)
        {
            torre.GetComponent<MeshRenderer>().sharedMaterial = matRojo;
        }
    }
    public void PintarTorresNaranja()
    {
        foreach (GameObject torre in torres)
        {
            torre.GetComponent<MeshRenderer>().sharedMaterial = matNaranja;
        }
    }
}
