using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using Unity.VisualScripting;
using UnityEngine;

public class Bandidos_Comportamiento : MonoBehaviour
{
    [Header("Propiedades")]
    [SerializeField] public float daņoCamioneta;
    [SerializeField] public float daņoJugador;
    [SerializeField] float cooldownAtaqueCamioneta;
    public bool dispara;
    private float timerAtaqueCamioneta;
    private bool puedeAtacarCamioneta;
    private Enemigo_Controllador_Movimiento movimiento;
    [Header("Disparo")]
    [SerializeField] GameObject arma;
    [SerializeField] GameObject bala;
    [SerializeField] float cooldownDisparo;
    private float timerDisparo;
    void Start()
    {
        puedeAtacarCamioneta = true;
        timerDisparo = cooldownDisparo;
        timerAtaqueCamioneta = cooldownAtaqueCamioneta;
        movimiento = GetComponent<Enemigo_Controllador_Movimiento>();
    }
    void Update()
    {
        if (movimiento.detectoJugador)
        {
            Timer();
        }
        if (puedeAtacarCamioneta)
        {
            DetectarCamioneta();
        }
        else
        {
            TimerAtaqueCamioneta();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Camioneta")
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoCamioneta(daņoCamioneta);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoJugador(daņoJugador);
        }
    }

    private void Timer()
    {
        timerDisparo -= Time.deltaTime;
        if (timerDisparo <= 0)
        {
            if (dispara)
            {
                RaycastHit hit;
                if (Physics.Raycast(arma.transform.position, transform.forward, out hit))
                {
                    if (hit.transform.CompareTag("Player"))
                    {
                        hit.transform.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoJugador(daņoJugador);
                    }
                }
            }
            Instantiate(bala, arma.transform.position, transform.rotation);
            timerDisparo = cooldownDisparo;
        }
    }
    private void TimerAtaqueCamioneta()
    {
        timerAtaqueCamioneta -= Time.deltaTime;
        if (timerAtaqueCamioneta <= 0)
        {
            puedeAtacarCamioneta = true;
            timerAtaqueCamioneta = cooldownAtaqueCamioneta;
        }
    }
    private void DetectarCamioneta()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 1))
        {
            if (hit.transform.gameObject.CompareTag("Camioneta"))
            {
                hit.transform.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoCamioneta(daņoCamioneta);
                puedeAtacarCamioneta = false;
            }
        }
    }
}
