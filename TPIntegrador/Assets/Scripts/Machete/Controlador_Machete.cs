using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Machete : MonoBehaviour
{
    public float daņo;
    private Player_HUD playerHud;
    private void Start()
    {
        playerHud = GameObject.Find("Jugador").GetComponent<Player_HUD>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemigo"))
        {
            other.gameObject.GetComponent<Controlador_Vida>().RecibirDaņoDesaparecer(daņo);
            other.gameObject.GetComponent<Controlador_Vida>().MostrarSangre(other.transform);
            if (other.gameObject.GetComponent<Controlador_Vida>().vidaActual <= 0)
            {
                other.gameObject.GetComponent<Enemigo_Dinero>().DarDinero();
                GestorDeAudio.instancia.ReproducirSonido("Killsound");
                playerHud.MostrarHitMarkerKill();
            }
            else
            {
                GestorDeAudio.instancia.ReproducirSonido("Hit Marker");
                playerHud.MostrarHitMarker();
            }
        }
        if (other.gameObject.CompareTag("Jefe"))
        {
            other.gameObject.GetComponent<Jefe_Vida>().RecibirDaņoDesaparecer(daņo);
            other.gameObject.GetComponent<Jefe_Vida>().MostrarSangre(other.transform);
            if (other.gameObject.GetComponent<Jefe_Vida>().vidaActual <= 0)
            {
                other.gameObject.GetComponent<Enemigo_Dinero>().DarDinero();
                GestorDeAudio.instancia.ReproducirSonido("Killsound");
                playerHud.MostrarHitMarkerKill();
            }
            else
            {
                GestorDeAudio.instancia.ReproducirSonido("Hit Marker");
                playerHud.MostrarHitMarker();
            }
        }
    }
}
